package com.cs.samkracustomer.Fragements;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.cs.samkracustomer.Activity.CamraActivity;
import com.cs.samkracustomer.Activity.HistoryActivity;
import com.cs.samkracustomer.Activity.InboxActivity;
import com.cs.samkracustomer.Activity.NotificationActivity;
import com.cs.samkracustomer.R;

import static com.cs.samkracustomer.Activity.MainActivity.drawer;

public class HomeFragment extends Fragment {

    ImageView captureIcon,menu_btn;
    ImageView homecamra, recipits, inbox, notification;
    View rootView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         rootView = inflater.inflate(R.layout.activity_home, container, false);

        menu_btn = (ImageView) rootView.findViewById(R.id.meu_btn);
        captureIcon =(ImageView) rootView.findViewById(R.id.capture_icon);
        recipits =(ImageView) rootView.findViewById(R.id.recipts);
        inbox =(ImageView) rootView.findViewById(R.id.inbox);
        notification =(ImageView) rootView.findViewById(R.id.home_notificaton);

        captureIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CamraActivity.class);
                startActivity(intent);
            }
        });

        inbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), InboxActivity.class);
                startActivity(intent);
            }
        });

        menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START);

            }
        });
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), NotificationActivity.class);
                startActivity(intent);
            }
        });
        recipits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), HistoryActivity.class);
                startActivity(intent);
            }
        });
        return rootView;
    }
}