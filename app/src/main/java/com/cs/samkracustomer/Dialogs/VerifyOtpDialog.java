package com.cs.samkracustomer.Dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkracustomer.Activity.MainActivity;
import com.cs.samkracustomer.Activity.SplashScreen;
import com.cs.samkracustomer.Models.Signupresponse;
import com.cs.samkracustomer.Models.VerifyMobileResponse;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;
import com.mukesh.OtpListener;
import com.mukesh.OtpView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import swarajsaaj.smscodereader.interfaces.OTPListener;
import swarajsaaj.smscodereader.receivers.OtpReader;

import static com.cs.samkracustomer.Activity.ForgotPasswordActivity.isResetSuccessful;

public class VerifyOtpDialog extends AppCompatActivity implements OTPListener, View.OnClickListener {

    String otpEntered="", strName, strEmail, strNameAr, strMobile, strPassword;
    Button buttonVerify;
    TextView buttonResend;
    //    ImageView imgEditMobile;
    String language;
    OtpView otpView;
    TextView textMobileNumber;;
    private String screen = "";
    private static String TAG = "TAG";
    CountDownTimer countDownTimer;
    SharedPreferences userPrefs;
    SharedPreferences LanguagePrefs;
    SharedPreferences.Editor userPrefsEditor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vefiry_otp);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        OtpReader.bind(this, "cs-test");

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        userPrefsEditor = userPrefs.edit();

        otpView = (OtpView) findViewById(R.id.otp_view);
        textMobileNumber = (TextView) findViewById(R.id.mobile);

        buttonVerify = (Button) findViewById(R.id.button_verify_otp);
        buttonResend = (TextView) findViewById(R.id.button_resend_otp);

        strName = getIntent().getStringExtra("name");
        strEmail = getIntent().getStringExtra("email");
        strMobile = getIntent().getStringExtra("mobile");
        strPassword = getIntent().getStringExtra("password");
        screen = getIntent().getStringExtra("screen");
        textMobileNumber.setText(Constants.Country_Code+strMobile);

        buttonResend.setEnabled(false);
        buttonResend.setAlpha(0.5f);
//        setTypeface();
        setTimerForResend();

        otpView.setListener(new OtpListener() {
            @Override
            public void onOtpEntered(String otp) {
                otpEntered = otp;
            }
        });

        buttonVerify.setOnClickListener(this);
        buttonResend.setOnClickListener(this);
//        imgEditMobile.setOnClickListener(this);
    }

    private void setTypeface(){
        Typeface typeface = Typeface.createFromAsset(getAssets(),
                "helvetica.ttf");

//        textBody1.setTypeface(typeface);
//        textBody2.setTypeface(typeface);
//        textBody3.setTypeface(typeface);
//        textOtpTitle.setTypeface(typeface);
        buttonResend.setTypeface(typeface);
        buttonVerify.setTypeface(typeface);
//        textMobileNumber.setTypeface(typeface);
    }

    private void setTimerForResend(){
        countDownTimer = new CountDownTimer(120000, 1000) {

            public void onTick(long millisUntilFinished) {
                String timeRemaining = String.format("%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % TimeUnit.HOURS.toMinutes(1),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % TimeUnit.MINUTES.toSeconds(1));
//                Log.i(TAG, "onTick: "+timeRemaining);
//                if(getDialog()!=null) {
                buttonResend.setText(getResources().getString(R.string.otp_msg_resend) + " in " + timeRemaining);
//                }
            }

            public void onFinish() {
//                if(getDialog()!=null) {
                buttonResend.setText(getResources().getString(R.string.otp_msg_resend));
                buttonResend.setEnabled(true);
                buttonResend.setAlpha(1.0f);
//                }
            }

        }.start();
    }

    @Override
    public void otpReceived(String messageText) {
        String[] str = null;
        try {
            messageText = messageText.replace("'","");
            str = messageText.split(":");
        } catch (Exception e) {
            e.printStackTrace();
        }
        otpView.setOTP(""+str[1]);
        otpEntered = str[1];
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_verify_otp:
                if(otpEntered.length() != 4){
                    Constants.showOneButtonAlertDialog(getResources().getString(R.string.otp_alert1),
                            getResources().getString(R.string.alert_invalid_otp), getResources().getString(R.string.ok), VerifyOtpDialog.this);
                }
                else{
                    if(screen.equals("register")){
                        String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOtpDialog.this);
                        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            new userRegistrationApi().execute();
                        }
                        else{
                            Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else if(screen.equals("forgot")){
//                        Intent i = new Intent(VerifyOtpDialog.this,ResetPasswordDialog.class);
//                        i.putExtra("mobile", strMobile);
//                        i.putExtra("otp", otpEntered);
//                        startActivity(i);
                        displayResetPasswordDiaolg();
                    }
                }
                break;

//            case R.id.edit_mobile_number:
////                SignUpActivity.isOTPVerified = false;
//                getDialog().cancel();
//                break;

            case R.id.button_resend_otp:
                String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOtpDialog.this);
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    new verifyMobileApi().execute();
                }
                else{
                    Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private String prepareVerifyMobileJson(){
        JSONObject mobileObj = new JSONObject();
        try {
            mobileObj.put("MobileNo","966"+strMobile);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mobileObj.toString();
    }

    private String prepareSignUpJson(){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Username", strName);
            parentObj.put("Email", strEmail);
            parentObj.put("Phone","966"+strMobile);
            parentObj.put("Password", strPassword);
            parentObj.put("Otp", otpEntered);
            parentObj.put("Language", language);
            parentObj.put("DeviceToken",SplashScreen.regId);

            Log.d(TAG, "prepareSignUpJson: "+parentObj.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return parentObj.toString();
    }

    private class userRegistrationApi extends AsyncTask<String, String, String> {

        ACProgressFlower dialog;
        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareSignUpJson();
            dialog = new ACProgressFlower.Builder(VerifyOtpDialog.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOtpDialog.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<Signupresponse> call = apiService.userRegistration(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<Signupresponse>() {
                @Override
                public void onResponse(Call<Signupresponse> call, Response<Signupresponse> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        Signupresponse registrationResponse = response.body();
                        if (registrationResponse.getStatus()) {
//                          status true case
                            String userId = registrationResponse.getData().getUserid();
                            userPrefsEditor.putString("userId", userId);
                            userPrefsEditor.putString("name", registrationResponse.getData().getUsername());
                            userPrefsEditor.putString("email", registrationResponse.getData().getEmail());
                            userPrefsEditor.putString("mobile", registrationResponse.getData().getPhone());
                            userPrefsEditor.commit();
                            Intent intent = new Intent(VerifyOtpDialog.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        } else {
//                          status false case
                            String failureResponse = registrationResponse.getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), VerifyOtpDialog.this);
                        }
                    } else {
                        if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<Signupresponse> call, Throwable t) {
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private class verifyMobileApi extends AsyncTask<String, String, String> {
        ACProgressFlower dialog;
        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            dialog = new ACProgressFlower.Builder(VerifyOtpDialog.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOtpDialog.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<VerifyMobileResponse> call = apiService.verfiyMobileNumber(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<VerifyMobileResponse>() {
                @Override
                public void onResponse(Call<VerifyMobileResponse> call, Response<VerifyMobileResponse> response) {
                    if(response.isSuccessful()){
                        VerifyMobileResponse verifyMobileResponse = response.body();
                        try {
                            if(verifyMobileResponse.getStatus()){
                                Log.i(TAG, "otp: "+verifyMobileResponse.getData().getOtp());
                            }
                            else {
                                String failureResponse = verifyMobileResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), VerifyOtpDialog.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if(dialog != null) {
                        dialog.dismiss();
                    }
                    buttonResend.setEnabled(false);
                    buttonResend.setAlpha(0.5f);
                    setTimerForResend();
                }

                @Override
                public void onFailure(Call<VerifyMobileResponse> call, Throwable t) {
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if(dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(countDownTimer!=null){
            countDownTimer.cancel();
        }
    }

    @Override
    public void onBackPressed() {

    }

    private void displayResetPasswordDiaolg(){
        Bundle args = new Bundle();
        args.putString("mobile", strMobile);
        args.putString("otp", otpEntered);

        isResetSuccessful = false;

        final ResetPasswordDialog newFragment = ResetPasswordDialog.newInstance();
        newFragment.setCancelable(false);
        newFragment.setArguments(args);
        newFragment.show(getSupportFragmentManager(), "forgot");

        getSupportFragmentManager().executePendingTransactions();
        newFragment.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //do whatever you want when dialog is dismissed
                if (newFragment != null) {
                    newFragment.dismiss();
                }

                if(isResetSuccessful){
                    Intent intent = new Intent(VerifyOtpDialog.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

}
