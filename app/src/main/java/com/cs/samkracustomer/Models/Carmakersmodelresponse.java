package com.cs.samkracustomer.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Carmakersmodelresponse {
    @Expose
    @SerializedName("Data")
    private ArrayList<Data> data = new ArrayList<>();
    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data {
        @Expose
        @SerializedName("mdl")
        private List<Mdl> mdl;
        @Expose
        @SerializedName("CarMakerName_ar")
        private String carmakernameAr;
        @Expose
        @SerializedName("CarMakerName_en")
        private String carmakernameEn;
        @Expose
        @SerializedName("CarMakerId")
        private int carmakerid;

        public List<Mdl> getMdl() {
            return mdl;
        }

        public void setMdl(List<Mdl> mdl) {
            this.mdl = mdl;
        }

        public String getCarmakernameAr() {
            return carmakernameAr;
        }

        public void setCarmakernameAr(String carmakernameAr) {
            this.carmakernameAr = carmakernameAr;
        }

        public String getCarmakernameEn() {
            return carmakernameEn;
        }

        public void setCarmakernameEn(String carmakernameEn) {
            this.carmakernameEn = carmakernameEn;
        }

        public int getCarmakerid() {
            return carmakerid;
        }

        public void setCarmakerid(int carmakerid) {
            this.carmakerid = carmakerid;
        }
    }

    public static class Mdl {
        @Expose
        @SerializedName("ModelName_ar")
        private String modelnameAr;
        @Expose
        @SerializedName("ModelName_en")
        private String modelnameEn;
        @Expose
        @SerializedName("ModelYear")
        private String modelyear;
        @Expose
        @SerializedName("CarMakerId")
        private int carmakerid;
        @Expose
        @SerializedName("ModelId")
        private int modelid;

        public String getModelnameAr() {
            return modelnameAr;
        }

        public void setModelnameAr(String modelnameAr) {
            this.modelnameAr = modelnameAr;
        }

        public String getModelnameEn() {
            return modelnameEn;
        }

        public void setModelnameEn(String modelnameEn) {
            this.modelnameEn = modelnameEn;
        }

        public String getModelyear() {
            return modelyear;
        }

        public void setModelyear(String modelyear) {
            this.modelyear = modelyear;
        }

        public int getCarmakerid() {
            return carmakerid;
        }

        public void setCarmakerid(int carmakerid) {
            this.carmakerid = carmakerid;
        }

        public int getModelid() {
            return modelid;
        }

        public void setModelid(int modelid) {
            this.modelid = modelid;
        }
    }
}
