package com.cs.samkracustomer.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class GetUserNewRequestResponse implements Serializable {


    @Expose
    @SerializedName("Data")
    private ArrayList<Data> data;
    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data implements Serializable {
        @Expose
        @SerializedName("CM")
        private ArrayList<CM> cm;
        @Expose
        @SerializedName("BidList")
        private ArrayList<Bidlist> bidlist;
        @Expose
        @SerializedName("RequestStatus")
        private String requeststatus;
        @Expose
        @SerializedName("RequestCount")
        private int requestcount;
        @Expose
        @SerializedName("CreatedOn")
        private String createdon;
        @Expose
        @SerializedName("Status")
        private boolean status;
        @Expose
        @SerializedName("RequestCode")
        private String requestcode;
        @Expose
        @SerializedName("UserComments")
        private String usercomments;
        @Expose
        @SerializedName("Address")
        private String address;
        @Expose
        @SerializedName("Year")
        private int year;
        @Expose
        @SerializedName("ModelId")
        private int modelid;
        @Expose
        @SerializedName("Longitude")
        private String longitude;
        @Expose
        @SerializedName("Latitude")
        private String latitude;
        @Expose
        @SerializedName("MakerId")
        private int makerid;
        @Expose
        @SerializedName("UserId")
        private int userid;
        @Expose
        @SerializedName("RequestId")
        private int requestid;

        public ArrayList<CM> getCm() {
            return cm;
        }

        public void setCm(ArrayList<CM> cm) {
            this.cm = cm;
        }

        public ArrayList<Bidlist> getBidlist() {
            return bidlist;
        }

        public void setBidlist(ArrayList<Bidlist> bidlist) {
            this.bidlist = bidlist;
        }

        public String getRequeststatus() {
            return requeststatus;
        }

        public void setRequeststatus(String requeststatus) {
            this.requeststatus = requeststatus;
        }

        public int getRequestcount() {
            return requestcount;
        }

        public void setRequestcount(int requestcount) {
            this.requestcount = requestcount;
        }

        public String getCreatedon() {
            return createdon;
        }

        public void setCreatedon(String createdon) {
            this.createdon = createdon;
        }

        public boolean getStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public String getRequestcode() {
            return requestcode;
        }

        public void setRequestcode(String requestcode) {
            this.requestcode = requestcode;
        }

        public String getUsercomments() {
            return usercomments;
        }

        public void setUsercomments(String usercomments) {
            this.usercomments = usercomments;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public int getYear() {
            return year;
        }

        public void setYear(int year) {
            this.year = year;
        }

        public int getModelid() {
            return modelid;
        }

        public void setModelid(int modelid) {
            this.modelid = modelid;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public int getMakerid() {
            return makerid;
        }

        public void setMakerid(int makerid) {
            this.makerid = makerid;
        }

        public int getUserid() {
            return userid;
        }

        public void setUserid(int userid) {
            this.userid = userid;
        }

        public int getRequestid() {
            return requestid;
        }

        public void setRequestid(int requestid) {
            this.requestid = requestid;
        }
    }

    public static class CM implements Serializable {
        @Expose
        @SerializedName("MDL")
        private ArrayList<MDL> mdl;
        @Expose
        @SerializedName("CarMakerNameAr")
        private String carmakernamear;
        @Expose
        @SerializedName("CarMakerNameEn")
        private String carmakernameen;
        @Expose
        @SerializedName("CarMakerId")
        private int carmakerid;

        public ArrayList<MDL> getMdl() {
            return mdl;
        }

        public void setMdl(ArrayList<MDL> mdl) {
            this.mdl = mdl;
        }

        public String getCarmakernamear() {
            return carmakernamear;
        }

        public void setCarmakernamear(String carmakernamear) {
            this.carmakernamear = carmakernamear;
        }

        public String getCarmakernameen() {
            return carmakernameen;
        }

        public void setCarmakernameen(String carmakernameen) {
            this.carmakernameen = carmakernameen;
        }

        public int getCarmakerid() {
            return carmakerid;
        }

        public void setCarmakerid(int carmakerid) {
            this.carmakerid = carmakerid;
        }
    }

    public static class MDL implements Serializable{
        @Expose
        @SerializedName("RB")
        private ArrayList<RB> rb;
        @Expose
        @SerializedName("ModelNameAr")
        private String modelnamear;
        @Expose
        @SerializedName("ModelNameEn")
        private String modelnameen;
        @Expose
        @SerializedName("ModelId")
        private int modelid;

        public ArrayList<RB> getRb() {
            return rb;
        }

        public void setRb(ArrayList<RB> rb) {
            this.rb = rb;
        }

        public String getModelnamear() {
            return modelnamear;
        }

        public void setModelnamear(String modelnamear) {
            this.modelnamear = modelnamear;
        }

        public String getModelnameen() {
            return modelnameen;
        }

        public void setModelnameen(String modelnameen) {
            this.modelnameen = modelnameen;
        }

        public int getModelid() {
            return modelid;
        }

        public void setModelid(int modelid) {
            this.modelid = modelid;
        }
    }

    public static class RB implements Serializable{
        @Expose
        @SerializedName("RCI")
        private ArrayList<RCI> rci;
        @Expose
        @SerializedName("UserFinalPriceAccepted")
        private boolean userfinalpriceaccepted;
        @Expose
        @SerializedName("FinalPrice")
        private float finalprice;
        @Expose
        @SerializedName("IsWSFinalPrice")
        private boolean iswsfinalprice;
        @Expose
        @SerializedName("BidOn")
        private String bidon;
        @Expose
        @SerializedName("MaxQuote")
        private float maxquote;
        @Expose
        @SerializedName("MinQuote")
        private float minquote;
        @Expose
        @SerializedName("WorkshopDistance")
        private int workshopdistance;
        @Expose
        @SerializedName("Longitude")
        private String longitude;
        @Expose
        @SerializedName("Latitude")
        private String latitude;
        @Expose
        @SerializedName("WorkshopId")
        private int workshopid;
        @Expose
        @SerializedName("BidId")
        private int bidid;
        @Expose
        @SerializedName("WorkShopComments")
        private String workshopcomments;
        @Expose
        @SerializedName("UserAcceptedComment")
        private String UserAcceptedComment;
        @Expose
        @SerializedName("FinalPriceWSComment")
        private String FinalPriceWSComment;

        public String getUserAcceptedComment() {
            return UserAcceptedComment;
        }

        public void setUserAcceptedComment(String userAcceptedComment) {
            UserAcceptedComment = userAcceptedComment;
        }

        public String getFinalPriceWSComment() {
            return FinalPriceWSComment;
        }

        public void setFinalPriceWSComment(String finalPriceWSComment) {
            FinalPriceWSComment = finalPriceWSComment;
        }

        public ArrayList<RCI> getRci() {
            return rci;
        }

        public void setRci(ArrayList<RCI> rci) {
            this.rci = rci;
        }

        public boolean getUserfinalpriceaccepted() {
            return userfinalpriceaccepted;
        }

        public void setUserfinalpriceaccepted(boolean userfinalpriceaccepted) {
            this.userfinalpriceaccepted = userfinalpriceaccepted;
        }

        public float getFinalprice() {
            return finalprice;
        }

        public void setFinalprice(float finalprice) {
            this.finalprice = finalprice;
        }

        public boolean getIswsfinalprice() {
            return iswsfinalprice;
        }

        public void setIswsfinalprice(boolean iswsfinalprice) {
            this.iswsfinalprice = iswsfinalprice;
        }

        public String getBidon() {
            return bidon;
        }

        public void setBidon(String bidon) {
            this.bidon = bidon;
        }

        public float getMaxquote() {
            return maxquote;
        }

        public void setMaxquote(float maxquote) {
            this.maxquote = maxquote;
        }

        public float getMinquote() {
            return minquote;
        }

        public void setMinquote(float minquote) {
            this.minquote = minquote;
        }

        public int getWorkshopdistance() {
            return workshopdistance;
        }

        public void setWorkshopdistance(int workshopdistance) {
            this.workshopdistance = workshopdistance;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public int getWorkshopid() {
            return workshopid;
        }

        public void setWorkshopid(int workshopid) {
            this.workshopid = workshopid;
        }

        public int getBidid() {
            return bidid;
        }

        public void setBidid(int bidid) {
            this.bidid = bidid;
        }

        public String getWorkshopcomments() {
            return workshopcomments;
        }

        public void setWorkshopcomments(String workshopcomments) {
            this.workshopcomments = workshopcomments;
        }
    }

    public static class RCI implements Serializable{
        @Expose
        @SerializedName("DocumentLocation")
        private String documentlocation;
        @Expose
        @SerializedName("DocumentType")
        private int documenttype;

        public String getDocumentlocation() {
            return documentlocation;
        }

        public void setDocumentlocation(String documentlocation) {
            this.documentlocation = documentlocation;
        }

        public int getDocumenttype() {
            return documenttype;
        }

        public void setDocumenttype(int documenttype) {
            this.documenttype = documenttype;
        }
    }

    public static class Bidlist implements Serializable{
        @Expose
        @SerializedName("wks")
        private ArrayList<Wks> wks;
        @Expose
        @SerializedName("WorkShopComments")
        private String workshopcomments;
        @Expose
        @SerializedName("MaxQuote")
        private float maxquote;
        @Expose
        @SerializedName("MinQuote")
        private float minquote;
        @Expose
        @SerializedName("RBID")
        private int rbid;
        @Expose
        @SerializedName("WorkshopId")
        private int workshopid;
        @Expose
        @SerializedName("RequestCode")
        private String requestcode;
        @Expose
        @SerializedName("RequestId")
        private int requestid;

        public ArrayList<Wks> getWks() {
            return wks;
        }

        public void setWks(ArrayList<Wks> wks) {
            this.wks = wks;
        }

        public String getWorkshopcomments() {
            return workshopcomments;
        }

        public void setWorkshopcomments(String workshopcomments) {
            this.workshopcomments = workshopcomments;
        }

        public float getMaxquote() {
            return maxquote;
        }

        public void setMaxquote(float maxquote) {
            this.maxquote = maxquote;
        }

        public float getMinquote() {
            return minquote;
        }

        public void setMinquote(float minquote) {
            this.minquote = minquote;
        }

        public int getRbid() {
            return rbid;
        }

        public void setRbid(int rbid) {
            this.rbid = rbid;
        }

        public int getWorkshopid() {
            return workshopid;
        }

        public void setWorkshopid(int workshopid) {
            this.workshopid = workshopid;
        }

        public String getRequestcode() {
            return requestcode;
        }

        public void setRequestcode(String requestcode) {
            this.requestcode = requestcode;
        }

        public int getRequestid() {
            return requestid;
        }

        public void setRequestid(int requestid) {
            this.requestid = requestid;
        }
    }

    public static class Wks implements Serializable{
        @Expose
        @SerializedName("BidOn")
        private String bidon;
        @Expose
        @SerializedName("Longitude")
        private String longitude;
        @Expose
        @SerializedName("Latitude")
        private String latitude;
        @Expose
        @SerializedName("WorkshopId")
        private int workshopid;
        @Expose
        @SerializedName("Address")
        private String address;
        @Expose
        @SerializedName("AverageRating")
        private int averagerating;
        @Expose
        @SerializedName("WorkshopDistance")
        private int workshopdistance;
        @Expose
        @SerializedName("AboutAr")
        private String aboutar;
        @Expose
        @SerializedName("AboutEn")
        private String abouten;
        @Expose
        @SerializedName("Logo")
        private String logo;
        @Expose
        @SerializedName("MobileNo")
        private String mobileno;
        @Expose
        @SerializedName("WorkshopName_ar")
        private String workshopnameAr;
        @Expose
        @SerializedName("WorkshopName_en")
        private String workshopnameEn;

        public String getBidon() {
            return bidon;
        }

        public void setBidon(String bidon) {
            this.bidon = bidon;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public int getWorkshopid() {
            return workshopid;
        }

        public void setWorkshopid(int workshopid) {
            this.workshopid = workshopid;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public int getAveragerating() {
            return averagerating;
        }

        public void setAveragerating(int averagerating) {
            this.averagerating = averagerating;
        }

        public int getWorkshopdistance() {
            return workshopdistance;
        }

        public void setWorkshopdistance(int workshopdistance) {
            this.workshopdistance = workshopdistance;
        }

        public String getAboutar() {
            return aboutar;
        }

        public void setAboutar(String aboutar) {
            this.aboutar = aboutar;
        }

        public String getAbouten() {
            return abouten;
        }

        public void setAbouten(String abouten) {
            this.abouten = abouten;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getMobileno() {
            return mobileno;
        }

        public void setMobileno(String mobileno) {
            this.mobileno = mobileno;
        }

        public String getWorkshopnameAr() {
            return workshopnameAr;
        }

        public void setWorkshopnameAr(String workshopnameAr) {
            this.workshopnameAr = workshopnameAr;
        }

        public String getWorkshopnameEn() {
            return workshopnameEn;
        }

        public void setWorkshopnameEn(String workshopnameEn) {
            this.workshopnameEn = workshopnameEn;
        }
    }
}
