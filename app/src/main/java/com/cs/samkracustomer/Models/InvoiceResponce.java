package com.cs.samkracustomer.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class InvoiceResponce implements Serializable {


    @Expose
    @SerializedName("Data")
    private ArrayList<Data> data;
    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data implements Serializable {
        @Expose
        @SerializedName("ws")
        private ArrayList<Ws> ws;
        @Expose
        @SerializedName("UserId")
        private int userid;
        @Expose
        @SerializedName("IsRating")
        private boolean israting;
        @Expose
        @SerializedName("Ratings")
        private int ratings;
        @Expose
        @SerializedName("UserFinalPriceAcceptedOn")
        private String userfinalpriceacceptedon;
        @Expose
        @SerializedName("WSFinalPriceOn")
        private String wsfinalpriceon;
        @Expose
        @SerializedName("AcceptedOn")
        private String acceptedon;
        @Expose
        @SerializedName("BidOn")
        private String bidon;
        @Expose
        @SerializedName("CreatedOn")
        private String createdon;
        @Expose
        @SerializedName("NetAmount")
        private int netamount;
        @Expose
        @SerializedName("VatCharges")
        private int vatcharges;
        @Expose
        @SerializedName("Discount")
        private int discount;
        @Expose
        @SerializedName("SubTotal")
        private int subtotal;
        @Expose
        @SerializedName("InvoiceNo")
        private String invoiceno;
        @Expose
        @SerializedName("WorkshopId")
        private int workshopid;
        @Expose
        @SerializedName("BidId")
        private int bidid;
        @Expose
        @SerializedName("RequestCode")
        private String requestcode;
        @Expose
        @SerializedName("RequestId")
        private int requestid;
        @Expose
        @SerializedName("InvoiceId")
        private int invoiceid;

        public ArrayList<Ws> getWs() {
            return ws;
        }

        public void setWs(ArrayList<Ws> ws) {
            this.ws = ws;
        }

        public int getUserid() {
            return userid;
        }

        public void setUserid(int userid) {
            this.userid = userid;
        }

        public boolean getIsrating() {
            return israting;
        }

        public void setIsrating(boolean israting) {
            this.israting = israting;
        }

        public int getRatings() {
            return ratings;
        }

        public void setRatings(int ratings) {
            this.ratings = ratings;
        }

        public String getUserfinalpriceacceptedon() {
            return userfinalpriceacceptedon;
        }

        public void setUserfinalpriceacceptedon(String userfinalpriceacceptedon) {
            this.userfinalpriceacceptedon = userfinalpriceacceptedon;
        }

        public String getWsfinalpriceon() {
            return wsfinalpriceon;
        }

        public void setWsfinalpriceon(String wsfinalpriceon) {
            this.wsfinalpriceon = wsfinalpriceon;
        }

        public String getAcceptedon() {
            return acceptedon;
        }

        public void setAcceptedon(String acceptedon) {
            this.acceptedon = acceptedon;
        }

        public String getBidon() {
            return bidon;
        }

        public void setBidon(String bidon) {
            this.bidon = bidon;
        }

        public String getCreatedon() {
            return createdon;
        }

        public void setCreatedon(String createdon) {
            this.createdon = createdon;
        }

        public int getNetamount() {
            return netamount;
        }

        public void setNetamount(int netamount) {
            this.netamount = netamount;
        }

        public int getVatcharges() {
            return vatcharges;
        }

        public void setVatcharges(int vatcharges) {
            this.vatcharges = vatcharges;
        }

        public int getDiscount() {
            return discount;
        }

        public void setDiscount(int discount) {
            this.discount = discount;
        }

        public int getSubtotal() {
            return subtotal;
        }

        public void setSubtotal(int subtotal) {
            this.subtotal = subtotal;
        }

        public String getInvoiceno() {
            return invoiceno;
        }

        public void setInvoiceno(String invoiceno) {
            this.invoiceno = invoiceno;
        }

        public int getWorkshopid() {
            return workshopid;
        }

        public void setWorkshopid(int workshopid) {
            this.workshopid = workshopid;
        }

        public int getBidid() {
            return bidid;
        }

        public void setBidid(int bidid) {
            this.bidid = bidid;
        }

        public String getRequestcode() {
            return requestcode;
        }

        public void setRequestcode(String requestcode) {
            this.requestcode = requestcode;
        }

        public int getRequestid() {
            return requestid;
        }

        public void setRequestid(int requestid) {
            this.requestid = requestid;
        }

        public int getInvoiceid() {
            return invoiceid;
        }

        public void setInvoiceid(int invoiceid) {
            this.invoiceid = invoiceid;
        }
    }

    public static class Ws implements Serializable {
        @Expose
        @SerializedName("usr")
        private ArrayList<Usr> usr;
        @Expose
        @SerializedName("Items")
        private ArrayList<Items> items;
        @Expose
        @SerializedName("WorkshopDistance")
        private int workshopdistance;
        @Expose
        @SerializedName("WorkshopNameAr")
        private String workshopnamear;
        @Expose
        @SerializedName("WorkshopNameEn")
        private String workshopnameen;

        public ArrayList<Usr> getUsr() {
            return usr;
        }

        public void setUsr(ArrayList<Usr> usr) {
            this.usr = usr;
        }

        public ArrayList<Items> getItems() {
            return items;
        }

        public void setItems(ArrayList<Items> items) {
            this.items = items;
        }

        public int getWorkshopdistance() {
            return workshopdistance;
        }

        public void setWorkshopdistance(int workshopdistance) {
            this.workshopdistance = workshopdistance;
        }

        public String getWorkshopnamear() {
            return workshopnamear;
        }

        public void setWorkshopnamear(String workshopnamear) {
            this.workshopnamear = workshopnamear;
        }

        public String getWorkshopnameen() {
            return workshopnameen;
        }

        public void setWorkshopnameen(String workshopnameen) {
            this.workshopnameen = workshopnameen;
        }
    }

    public static class Usr implements Serializable{
        @Expose
        @SerializedName("CM")
        private ArrayList<CM> cm;
        @Expose
        @SerializedName("UserId")
        private int userid;
        @Expose
        @SerializedName("UserName")
        private String username;

        public ArrayList<CM> getCm() {
            return cm;
        }

        public void setCm(ArrayList<CM> cm) {
            this.cm = cm;
        }

        public int getUserid() {
            return userid;
        }

        public void setUserid(int userid) {
            this.userid = userid;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }
    }

    public static class CM implements Serializable{
        @Expose
        @SerializedName("MDL")
        private ArrayList<MDL> mdl;
        @Expose
        @SerializedName("CarMakerNameAr")
        private String carmakernamear;
        @Expose
        @SerializedName("CarMakerNameEn")
        private String carmakernameen;
        @Expose
        @SerializedName("CarMakerId")
        private int carmakerid;

        public ArrayList<MDL> getMdl() {
            return mdl;
        }

        public void setMdl(ArrayList<MDL> mdl) {
            this.mdl = mdl;
        }

        public String getCarmakernamear() {
            return carmakernamear;
        }

        public void setCarmakernamear(String carmakernamear) {
            this.carmakernamear = carmakernamear;
        }

        public String getCarmakernameen() {
            return carmakernameen;
        }

        public void setCarmakernameen(String carmakernameen) {
            this.carmakernameen = carmakernameen;
        }

        public int getCarmakerid() {
            return carmakerid;
        }

        public void setCarmakerid(int carmakerid) {
            this.carmakerid = carmakerid;
        }
    }

    public static class MDL implements Serializable{
        @Expose
        @SerializedName("RCI")
        private ArrayList<RCI> rci;
        @Expose
        @SerializedName("ModelYear")
        private String modelyear;
        @Expose
        @SerializedName("ModelNameAr")
        private String modelnamear;
        @Expose
        @SerializedName("ModelNameEn")
        private String modelnameen;
        @Expose
        @SerializedName("ModelId")
        private int modelid;

        public ArrayList<RCI> getRci() {
            return rci;
        }

        public void setRci(ArrayList<RCI> rci) {
            this.rci = rci;
        }

        public String getModelyear() {
            return modelyear;
        }

        public void setModelyear(String modelyear) {
            this.modelyear = modelyear;
        }

        public String getModelnamear() {
            return modelnamear;
        }

        public void setModelnamear(String modelnamear) {
            this.modelnamear = modelnamear;
        }

        public String getModelnameen() {
            return modelnameen;
        }

        public void setModelnameen(String modelnameen) {
            this.modelnameen = modelnameen;
        }

        public int getModelid() {
            return modelid;
        }

        public void setModelid(int modelid) {
            this.modelid = modelid;
        }
    }

    public static class RCI implements Serializable{
        @Expose
        @SerializedName("DocumentLocation")
        private String documentlocation;
        @Expose
        @SerializedName("DocumentType")
        private int documenttype;

        public String getDocumentlocation() {
            return documentlocation;
        }

        public void setDocumentlocation(String documentlocation) {
            this.documentlocation = documentlocation;
        }

        public int getDocumenttype() {
            return documenttype;
        }

        public void setDocumenttype(int documenttype) {
            this.documenttype = documenttype;
        }
    }

    public static class Items implements Serializable{
        @Expose
        @SerializedName("ItemTotal")
        private String itemtotal;
        @Expose
        @SerializedName("UnitPrice")
        private String unitprice;
        @Expose
        @SerializedName("Quantity")
        private int quantity;
        @Expose
        @SerializedName("Item")
        private String item;
        @Expose
        @SerializedName("InvoiceId")
        private int invoiceid;
        @Expose
        @SerializedName("ItemId")
        private int itemid;

        public String getItemtotal() {
            return itemtotal;
        }

        public void setItemtotal(String itemtotal) {
            this.itemtotal = itemtotal;
        }

        public String getUnitprice() {
            return unitprice;
        }

        public void setUnitprice(String unitprice) {
            this.unitprice = unitprice;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public String getItem() {
            return item;
        }

        public void setItem(String item) {
            this.item = item;
        }

        public int getInvoiceid() {
            return invoiceid;
        }

        public void setInvoiceid(int invoiceid) {
            this.invoiceid = invoiceid;
        }

        public int getItemid() {
            return itemid;
        }

        public void setItemid(int itemid) {
            this.itemid = itemid;
        }
    }
}
