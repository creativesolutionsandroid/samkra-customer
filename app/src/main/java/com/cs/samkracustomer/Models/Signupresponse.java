package com.cs.samkracustomer.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class Signupresponse {
    @Expose
    @SerializedName("Data")
    private Data data;
    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data {
        @Expose
        @SerializedName("UserId")
        private String userid;
        @Expose
        @SerializedName("Language")
        private String language;
        @Expose
        @SerializedName("Username")
        private String username;
        @Expose
        @SerializedName("Email")
        private String email;
        @Expose
        @SerializedName("Phone")
        private String phone;

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }
}
