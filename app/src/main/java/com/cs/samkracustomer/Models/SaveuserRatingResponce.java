package com.cs.samkracustomer.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public abstract class SaveuserRatingResponce  implements Serializable {
    @Expose
    @SerializedName("Data")
    private Data data;
    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data  implements Serializable {
        @Expose
        @SerializedName("RequestId")
        private int requestid;

        public int getRequestid() {
            return requestid;
        }

        public void setRequestid(int requestid) {
            this.requestid = requestid;
        }
    }
}
