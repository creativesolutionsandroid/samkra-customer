package com.cs.samkracustomer.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public  class NotificationResponce implements Serializable {


    @Expose
    @SerializedName("Data")
    private ArrayList<Data> data;
    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data implements Serializable {
        @Expose
        @SerializedName("SentDate")
        private String sentdate;
        @Expose
        @SerializedName("ScheduleDate")
        private String scheduledate;
        @Expose
        @SerializedName("TitleAr")
        private String titlear;
        @Expose
        @SerializedName("TitleEn")
        private String titleen;
        @Expose
        @SerializedName("NotificationType")
        private int notificationtype;
        @Expose
        @SerializedName("MessageAr")
        private String messagear;
        @Expose
        @SerializedName("MessageEn")
        private String messageen;
        @Expose
        @SerializedName("UserType")
        private int usertype;
        @Expose
        @SerializedName("UserId")
        private int userid;

        public String getSentdate() {
            return sentdate;
        }

        public void setSentdate(String sentdate) {
            this.sentdate = sentdate;
        }

        public String getScheduledate() {
            return scheduledate;
        }

        public void setScheduledate(String scheduledate) {
            this.scheduledate = scheduledate;
        }

        public String getTitlear() {
            return titlear;
        }

        public void setTitlear(String titlear) {
            this.titlear = titlear;
        }

        public String getTitleen() {
            return titleen;
        }

        public void setTitleen(String titleen) {
            this.titleen = titleen;
        }

        public int getNotificationtype() {
            return notificationtype;
        }

        public void setNotificationtype(int notificationtype) {
            this.notificationtype = notificationtype;
        }

        public String getMessagear() {
            return messagear;
        }

        public void setMessagear(String messagear) {
            this.messagear = messagear;
        }

        public String getMessageen() {
            return messageen;
        }

        public void setMessageen(String messageen) {
            this.messageen = messageen;
        }

        public int getUsertype() {
            return usertype;
        }

        public void setUsertype(int usertype) {
            this.usertype = usertype;
        }

        public int getUserid() {
            return userid;
        }

        public void setUserid(int userid) {
            this.userid = userid;
        }
    }
}