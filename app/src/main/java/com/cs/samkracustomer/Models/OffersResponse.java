package com.cs.samkracustomer.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class OffersResponse implements Serializable {

    @Expose
    @SerializedName("Data")
    private ArrayList<Data> data = new ArrayList<>();
    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data implements Serializable{
        @Expose
        @SerializedName("wk")
        private ArrayList<Wk> wk = new ArrayList<>();
        @Expose
        @SerializedName("WorkShopComments")
        private String workshopcomments;
        @Expose
        @SerializedName("MaxQuote")
        private double maxquote;
        @Expose
        @SerializedName("MinQuote")
        private double minquote;
        @Expose
        @SerializedName("RBID")
        private int rbid;
        @Expose
        @SerializedName("WorkshopId")
        private int workshopid;
        @Expose
        @SerializedName("RequestId")
        private int requestid;
        @Expose
        @SerializedName("RequestCode")
        private String RequestCode;

        public String getRequestCode() {
            return RequestCode;
        }

        public void setRequestCode(String requestCode) {
            RequestCode = requestCode;
        }

        public ArrayList<Wk> getWk() {
            return wk;
        }

        public void setWk(ArrayList<Wk> wk) {
            this.wk = wk;
        }

        public String getWorkshopcomments() {
            return workshopcomments;
        }

        public void setWorkshopcomments(String workshopcomments) {
            this.workshopcomments = workshopcomments;
        }

        public double getMaxquote() {
            return maxquote;
        }

        public void setMaxquote(double maxquote) {
            this.maxquote = maxquote;
        }

        public double getMinquote() {
            return minquote;
        }

        public void setMinquote(double minquote) {
            this.minquote = minquote;
        }

        public int getRbid() {
            return rbid;
        }

        public void setRbid(int rbid) {
            this.rbid = rbid;
        }

        public int getWorkshopid() {
            return workshopid;
        }

        public void setWorkshopid(int workshopid) {
            this.workshopid = workshopid;
        }

        public int getRequestid() {
            return requestid;
        }

        public void setRequestid(int requestid) {
            this.requestid = requestid;
        }
    }

    public static class Wk implements Serializable{
        @Expose
        @SerializedName("wr")
        private ArrayList<Wr> wr = new ArrayList<>();
        @Expose
        @SerializedName("WorkshopId")
        private int workshopid;
        @Expose
        @SerializedName("MobileNo")
        private String MobileNo;
        @Expose
        @SerializedName("AverageRating")
        private float averagerating;
        @Expose
        @SerializedName("WorkshopDistance")
        private float workshopdistance;
        @Expose
        @SerializedName("AboutAr")
        private String aboutar;
        @Expose
        @SerializedName("AboutEn")
        private String abouten;
        @Expose
        @SerializedName("Logo")
        private String logo;
        @Expose
        @SerializedName("Address")
        private String address;
        @Expose
        @SerializedName("WorkshopName_ar")
        private String workshopnameAr;
        @Expose
        @SerializedName("WorkshopName_en")
        private String workshopnameEn;

        public String getMobileNo() {
            return MobileNo;
        }

        public void setMobileNo(String mobileNo) {
            MobileNo = mobileNo;
        }

        public ArrayList<Wr> getWr() {
            return wr;
        }

        public void setWr(ArrayList<Wr> wr) {
            this.wr = wr;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public int getWorkshopid() {
            return workshopid;
        }

        public void setWorkshopid(int workshopid) {
            this.workshopid = workshopid;
        }

        public float getAveragerating() {
            return averagerating;
        }

        public void setAveragerating(float averagerating) {
            this.averagerating = averagerating;
        }

        public float getWorkshopdistance() {
            return workshopdistance;
        }

        public void setWorkshopdistance(float workshopdistance) {
            this.workshopdistance = workshopdistance;
        }

        public String getAboutar() {
            return aboutar;
        }

        public void setAboutar(String aboutar) {
            this.aboutar = aboutar;
        }

        public String getAbouten() {
            return abouten;
        }

        public void setAbouten(String abouten) {
            this.abouten = abouten;
        }

        public String getWorkshopnameAr() {
            return workshopnameAr;
        }

        public void setWorkshopnameAr(String workshopnameAr) {
            this.workshopnameAr = workshopnameAr;
        }

        public String getWorkshopnameEn() {
            return workshopnameEn;
        }

        public void setWorkshopnameEn(String workshopnameEn) {
            this.workshopnameEn = workshopnameEn;
        }
    }

    public static class Wr implements Serializable{
        @Expose
        @SerializedName("BidOn")
        private String bidon;

        @Expose
        @SerializedName("Username")
        private String Username;

        @Expose
        @SerializedName("Comments")
        private String Comments;

        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;

        @Expose
        @SerializedName("Rating")
        private String Rating;

        public String getUsername() {
            return Username;
        }

        public void setUsername(String username) {
            Username = username;
        }

        public String getComments() {
            return Comments;
        }

        public void setComments(String comments) {
            Comments = comments;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String createdDate) {
            CreatedDate = createdDate;
        }

        public String getRating() {
            return Rating;
        }

        public void setRating(String rating) {
            Rating = rating;
        }

        public String getBidon() {
            return bidon;
        }

        public void setBidon(String bidon) {
            this.bidon = bidon;
        }
    }
}
