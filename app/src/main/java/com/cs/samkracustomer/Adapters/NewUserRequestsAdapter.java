package com.cs.samkracustomer.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.samkracustomer.Models.GetUserNewRequestResponse;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Utils.Constants;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
public class NewUserRequestsAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<GetUserNewRequestResponse.Data> historyArrayList = new ArrayList<>();
    String language;

    public NewUserRequestsAdapter(Context context, ArrayList<GetUserNewRequestResponse.Data> orderList, String language) {
        this.context = context;
        this.historyArrayList = orderList;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return historyArrayList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView tvCarName, tvcarModel, tvDate, tvTime;
        ImageView carImage;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.requests_child, null);
//            }
//            else{
//                convertView = inflater.inflate(R.layout.list_host_history_ar, null);
//            }

            holder.tvCarName = (TextView) convertView.findViewById(R.id.carname);
            holder.tvcarModel = (TextView) convertView.findViewById(R.id.carmodel);
            holder.tvDate = (TextView) convertView.findViewById(R.id.date);
            holder.tvTime = (TextView) convertView.findViewById(R.id.time);

            holder.carImage = (ImageView) convertView.findViewById(R.id.caricon);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        try {
            Glide.with(context)
                    .load(Constants.IMAGE_URL+historyArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(0).getDocumentlocation())
                    .into(holder.carImage);
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.tvCarName.setText(historyArrayList.get(position).getCm().get(0).getCarmakernameen());
        holder.tvcarModel.setText(historyArrayList.get(position).getCm().get(0).getMdl().get(0).getModelnameen());

        String str = historyArrayList.get(position).getCreatedon();
        String[] array = str.split(" ");

        holder.tvDate.setText(array[0]);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm a");

        String tt = array[1];
        try {
            Date time = sdf.parse(tt);
            tt = sdf1.format(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.tvTime.setText(tt);

        return convertView;
    }
}