package com.cs.samkracustomer.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.samkracustomer.Activity.CamraActivity;
import com.cs.samkracustomer.R;

import java.util.ArrayList;

/**
 * Created by BRAHMAM on 21-11-2015.
 */
public class ImagesUploadedAdapter extends PagerAdapter {

    LayoutInflater mLayoutInflater;
    ArrayList<Bitmap> images;
    Context context;


    public ImagesUploadedAdapter(Context context, ArrayList<Bitmap> images) {
        this.context = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.images = images;
    }

    @Override
    public float getPageWidth(int position) {
        float nbPages = 3.2f; // You could display partial pages using a float value
        return (1 / nbPages);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.list_image, container, false);

        ImageView store_image = (ImageView) itemView.findViewById(R.id.image);
        store_image.setImageBitmap(images.get(position));

        store_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CamraActivity.imageView.setImageBitmap(images.get(position));
                CamraActivity.selectedBitmap = images.get(position);
            }
        });
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}