package com.cs.samkracustomer.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cs.samkracustomer.Models.AccpetedOffersResponce;
import com.cs.samkracustomer.Models.InvoiceResponce;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Utils.Constants;

import java.util.ArrayList;

public class InvoiceItemAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<AccpetedOffersResponce.Items> invoiceArrayList = new ArrayList<>();
    String language;
    int invoicepos;

    public InvoiceItemAdapter(Context context, ArrayList<AccpetedOffersResponce.Items> orderList, int invoicepos, String language) {
        this.context = context;
        this.invoiceArrayList = orderList;
        this.invoicepos = invoicepos;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return invoiceArrayList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView mitem,amt;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")) {
            convertView = inflater.inflate(R.layout.invoice_list, null);
//            }
//            else{
//                convertView = inflater.inflate(R.layout.list_host_history_ar, null);
//            }

            holder.mitem = (TextView) convertView.findViewById(R.id.item);
            holder.amt = (TextView) convertView.findViewById(R.id.amt);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.mitem.setText(""+invoiceArrayList.get(position).getItem());
        holder.amt.setText(Constants.priceFormat.format(invoiceArrayList.get(position).getItemtotal()));


        return convertView;
    }



}
