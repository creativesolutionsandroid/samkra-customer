package com.cs.samkracustomer.Adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.cs.samkracustomer.Models.GetUserNewRequestResponse;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Utils.Constants;

import java.util.ArrayList;

public class ViewPageAdapter extends PagerAdapter {

    private LayoutInflater inflater;
    private static Context context;
    int pos, type;
    String language;
    ImageView mimg;
    ArrayList<GetUserNewRequestResponse.RCI> img = new ArrayList<>();
    private static String TAG = "TAG";
    private int currentPos = 0;

    public ViewPageAdapter(Context context, ArrayList<GetUserNewRequestResponse.RCI> img, int type, int pos, String language) {
        this.context = context;
        this.type = type;
        this.img = img;
        this.pos = pos;
        this.language = language;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
//        if(youtubeFragment != null){
////            youtubeFragment.loadUrl("");
//            youtubeFragment.stopLoading();
////            youtubeFragment.destroy();
//        }
        container.removeView((ViewGroup) object);
    }

//    public void destroyItem(ViewGroup container, int position, Object object) {
//        destroyItem((View) container, position, object);
//    }

    public void destroyItem(View container, int position, Object object) {
        try {
            throw new UnsupportedOperationException("Required method destroyItem was not overridden");
        } catch (Exception e) {

        }
        destroyItem((ViewGroup) container, position, object);
    }

    @Override
    public int getCount() {
        Log.d("TAG", "getCount: " + img.size());
        return img.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        Log.d("TAG", "instantiateItem: ");
        View myImageLayout = inflater.inflate(R.layout.inbox_sub_list, view, false);
        mimg = (ImageView) myImageLayout
                .findViewById(R.id.img);

//        if (language.equalsIgnoreCase("Ar")) {
//            layout.setRotationY(180);
//        }


        if (img.get(position).getDocumenttype() == type) {


            Glide.with(context).load(Constants.IMAGE_URL + img.get(position).getDocumentlocation()).into(mimg);

        }
        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

}
