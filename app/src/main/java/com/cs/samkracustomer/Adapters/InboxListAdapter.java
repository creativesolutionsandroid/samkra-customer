package com.cs.samkracustomer.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.samkracustomer.Activity.FinalPrice_Bid;
import com.cs.samkracustomer.Activity.InboxActivity;
import com.cs.samkracustomer.Activity.OffersActivity;
import com.cs.samkracustomer.Models.AccpetedOffersResponce;
import com.cs.samkracustomer.Models.GetUserNewRequestResponse;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Utils.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class InboxListAdapter extends BaseAdapter{

    public Context context;
    public LayoutInflater inflater;
    ArrayList<GetUserNewRequestResponse.Data> inboxArrayList = new ArrayList<>();
    String language;
    ViewPageAdapter viewPageAdapter;
    int type;

    public InboxListAdapter(Context context, ArrayList<GetUserNewRequestResponse.Data> orderList, String language) {
        this.context = context;
        this.inboxArrayList = orderList;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return inboxArrayList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView mrequest_code, mdate, mcar_right, mcar_left, mcar_front, mcar_back, car_make, car_model, car_year, bids_count, min_price, max_price, final_price;
        LinearLayout mprice_layout, back_layout, front_layout, left_layout, right_layout;
        RelativeLayout mbids_count_layout;
        ViewPager img_list;
        ImageView back_side, front_side, left_side, right_side, dropdown;
        LinearLayout imagesLayout;
        Button button;
//        RelativeLayout mainLayout;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")) {
            convertView = inflater.inflate(R.layout.inbox_list, null);
//            }
//            else{
//                convertView = inflater.inflate(R.layout.list_host_history_ar, null);
//            }

            holder.mrequest_code = (TextView) convertView.findViewById(R.id.request_code);
            holder.mdate = (TextView) convertView.findViewById(R.id.date);
//            holder.mcar_back = (TextView) convertView.findViewById(R.id.car_back);
//            holder.mcar_left = (TextView) convertView.findViewById(R.id.car_left);
//            holder.mcar_right = (TextView) convertView.findViewById(R.id.car_right);
//            holder.mcar_front = (TextView) convertView.findViewById(R.id.car_front);
            holder.car_make = (TextView) convertView.findViewById(R.id.car_make);
            holder.car_model = (TextView) convertView.findViewById(R.id.car_model);
            holder.car_year = (TextView) convertView.findViewById(R.id.car_year);
            holder.bids_count = (TextView) convertView.findViewById(R.id.bids_count);
            holder.min_price = (TextView) convertView.findViewById(R.id.min_price);
            holder.max_price = (TextView) convertView.findViewById(R.id.max_price);
            holder.final_price = (TextView) convertView.findViewById(R.id.final_price);
            holder.button = (Button) convertView.findViewById(R.id.button);

            holder.mbids_count_layout = (RelativeLayout) convertView.findViewById(R.id.bids_count_layout);
//            holder.mainLayout = (RelativeLayout) convertView.findViewById(R.id.layout);

            holder.mprice_layout = (LinearLayout) convertView.findViewById(R.id.price_layount);
            holder.back_layout = (LinearLayout) convertView.findViewById(R.id.back_layout);
            holder.left_layout = (LinearLayout) convertView.findViewById(R.id.left_layout);
            holder.right_layout = (LinearLayout) convertView.findViewById(R.id.right_layout);
            holder.front_layout = (LinearLayout) convertView.findViewById(R.id.front_layout);

            holder.img_list = (ViewPager) convertView.findViewById(R.id.view_pager);

            holder.back_side = (ImageView) convertView.findViewById(R.id.back_side);
            holder.front_side = (ImageView) convertView.findViewById(R.id.front_side);
            holder.left_side = (ImageView) convertView.findViewById(R.id.left_side);
            holder.right_side = (ImageView) convertView.findViewById(R.id.right_side);
            holder.dropdown = (ImageView) convertView.findViewById(R.id.down);
            holder.imagesLayout = (LinearLayout) convertView.findViewById(R.id.images_layout);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.mrequest_code.setText("#"+inboxArrayList.get(position).getRequestid());

        String date = inboxArrayList.get(position).getCreatedon();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy hh:mm a");

        try {
            Date datetime = sdf.parse(date);
            date = sdf1.format(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.mdate.setText(""+date);

        holder.car_make.setText(""+inboxArrayList.get(position).getCm().get(0).getCarmakernameen());
        holder.car_model.setText(""+inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getModelnameen());
        holder.car_year.setText(""+inboxArrayList.get(position).getYear());

        ArrayList<String> images = new ArrayList<>();
//        boolean isRight = false;
//        boolean isLeft = false;
//        boolean isFront = false;
//        boolean isBack = false;
//        for (int i = 0; i < inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().size(); i++) {
//            if (inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(0).getDocumenttype() == 1) {
//                isRight = true;
//            }
//            else if (inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(0).getDocumenttype() == 2) {
//                isLeft = true;
//            }
//            else if (inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(0).getDocumenttype() == 3) {
//                isFront = true;
//            }
//            else if (inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(0).getDocumenttype() == 4) {
//                isBack = true;
//            }
//
//            Log.d("TAG", "getView: "+position+": "+isRight+","+isLeft+","+isFront+","+isBack);
//        }

//        if(!isRight){
//            holder.right_layout.setEnabled(false);
//            holder.right_side.setImageDrawable(context.getResources().getDrawable(R.drawable.right));
//        }
//        else{
//            holder.right_layout.setEnabled(true);
//            holder.right_side.setImageDrawable(context.getResources().getDrawable(R.drawable.right_selected));
//            for (int i = 0; i < inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().size(); i++) {
//                if (inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(0).getDocumenttype() == 1) {
//                    images.add(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(0).getDocumentlocation());
//                }
//                else {
//
//                }
//            }
//        }
//
//        if(!isLeft){
//            holder.left_layout.setEnabled(false);
//            holder.left_side.setImageDrawable(context.getResources().getDrawable(R.drawable.left));
//        }
//        else{
//            holder.left_layout.setEnabled(true);
//            if(!isRight) {
//                holder.left_side.setImageDrawable(context.getResources().getDrawable(R.drawable.left_selected));
//                for (int i = 0; i < inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().size(); i++) {
//                    if (inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(0).getDocumenttype() == 2) {
//                        images.add(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(0).getDocumentlocation());
//                    } else {
//
//                    }
//                }
//            }
//            else{
//
//            }
//        }
//
//        if(!isFront){
//            holder.front_layout.setEnabled(false);
//            holder.front_side.setImageDrawable(context.getResources().getDrawable(R.drawable.front));
//        }
//        else{
//            holder.front_layout.setEnabled(true);
//            if(!isRight && !isLeft) {
//                holder.front_side.setImageDrawable(context.getResources().getDrawable(R.drawable.front_selected));
//                for (int i = 0; i < inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().size(); i++) {
//                    if (inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(0).getDocumenttype() == 3) {
//                        images.add(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(0).getDocumentlocation());
//                    } else {
//
//                    }
//                }
//            }
//            else{
//
//            }
//        }
//
//        if(!isBack){
//            holder.back_side.setEnabled(false);
//            holder.back_side.setImageDrawable(context.getResources().getDrawable(R.drawable.back_car));
//        }
//        else{
//            holder.back_layout.setEnabled(true);
//            if(!isRight && !isLeft && !isFront) {
//                holder.back_side.setImageDrawable(context.getResources().getDrawable(R.drawable.back_selected));
//                for (int i = 0; i < inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().size(); i++) {
//                    if (inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(0).getDocumenttype() == 4) {
//                        images.add(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(0).getDocumentlocation());
//                    } else {
//
//                    }
//                }
//            }
//            else{
//
//            }
//        }

        for (int i = 0; i < inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().size(); i++) {
            images.add(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(0).getDocumentlocation());
        }

        holder.imagesLayout.removeAllViews();
        for (int i = 0; i < images.size(); i++) {
            View v = inflater.inflate(R.layout.list_image, null);
            ImageView imageView = v.findViewById(R.id.image);
            Glide.with(context).load(Constants.IMAGE_URL+images.get(i)).into(imageView);
            holder.imagesLayout.addView(v);
        }

        holder.right_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.front_side.setImageDrawable(context.getResources().getDrawable(R.drawable.front));
                holder.back_side.setImageDrawable(context.getResources().getDrawable(R.drawable.back_car));
                holder.left_side.setImageDrawable(context.getResources().getDrawable(R.drawable.left));
                holder.right_side.setImageDrawable(context.getResources().getDrawable(R.drawable.right_selected));

                ArrayList<String> images = new ArrayList<>();
                for (int i = 0; i < inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().size(); i++) {
                    if (inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(0).getDocumenttype() == 1) {
                        images.add(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(0).getDocumentlocation());
                    }
                    else {

                    }
                }

                holder.imagesLayout.removeAllViews();
                for (int i = 0; i < images.size(); i++) {
                    View v = inflater.inflate(R.layout.list_image, null);
                    ImageView imageView = v.findViewById(R.id.image);
                    Glide.with(context).load(Constants.IMAGE_URL+images.get(i)).into(imageView);
                    holder.imagesLayout.addView(v);
                }

            }
        });

        holder.left_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.front_side.setImageDrawable(context.getResources().getDrawable(R.drawable.front));
                holder.back_side.setImageDrawable(context.getResources().getDrawable(R.drawable.back_car));
                holder.left_side.setImageDrawable(context.getResources().getDrawable(R.drawable.left_selected));
                holder.right_side.setImageDrawable(context.getResources().getDrawable(R.drawable.right));

                ArrayList<String> images = new ArrayList<>();
                for (int i = 0; i < inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().size(); i++) {
                    if (inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(0).getDocumenttype() == 2) {
                        images.add(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(0).getDocumentlocation());
                    }
                    else {

                    }
                }

                holder.imagesLayout.removeAllViews();
                for (int i = 0; i < images.size(); i++) {
                    View v = inflater.inflate(R.layout.list_image, null);
                    ImageView imageView = v.findViewById(R.id.image);
                    Glide.with(context).load(Constants.IMAGE_URL+images.get(i)).into(imageView);
                    holder.imagesLayout.addView(v);
                }

            }
        });

        holder.front_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.front_side.setImageDrawable(context.getResources().getDrawable(R.drawable.front_selected));
                holder.back_side.setImageDrawable(context.getResources().getDrawable(R.drawable.back_car));
                holder.left_side.setImageDrawable(context.getResources().getDrawable(R.drawable.left));
                holder.right_side.setImageDrawable(context.getResources().getDrawable(R.drawable.right));

                ArrayList<String> images = new ArrayList<>();
                for (int i = 0; i < inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().size(); i++) {
                    if (inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(0).getDocumenttype() == 3) {
                        images.add(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(0).getDocumentlocation());
                    }
                    else {

                    }
                }

                holder.imagesLayout.removeAllViews();
                for (int i = 0; i < images.size(); i++) {
                    View v = inflater.inflate(R.layout.list_image, null);
                    ImageView imageView = v.findViewById(R.id.image);
                    Glide.with(context).load(Constants.IMAGE_URL+images.get(i)).into(imageView);
                    holder.imagesLayout.addView(v);
                }

            }
        });

        holder.back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.front_side.setImageDrawable(context.getResources().getDrawable(R.drawable.front));
                holder.back_side.setImageDrawable(context.getResources().getDrawable(R.drawable.back_selected));
                holder.left_side.setImageDrawable(context.getResources().getDrawable(R.drawable.left));
                holder.right_side.setImageDrawable(context.getResources().getDrawable(R.drawable.right));
                ArrayList<String> images = new ArrayList<>();
                for (int i = 0; i < inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().size(); i++) {
                    if (inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(0).getDocumenttype() == 4) {
                        images.add(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(0).getDocumentlocation());
                    }
                    else {

                    }
                }

                holder.imagesLayout.removeAllViews();
                for (int i = 0; i < images.size(); i++) {
                    View v = inflater.inflate(R.layout.list_image, null);
                    ImageView imageView = v.findViewById(R.id.image);
                    Glide.with(context).load(Constants.IMAGE_URL+images.get(i)).into(imageView);
                    holder.imagesLayout.addView(v);
                }
            }
        });

        if (inboxArrayList.get(position).getRequeststatus().equals("Pending")){

            holder.mbids_count_layout.setVisibility(View.VISIBLE);
            holder.mprice_layout.setVisibility(View.GONE);
            holder.button.setVisibility(View.GONE);

            holder.bids_count.setText(""+inboxArrayList.get(position).getRequestcount());

        } else if (inboxArrayList.get(position).getRequeststatus().equals("Accepted")) {

            holder.mbids_count_layout.setVisibility(View.GONE);
            holder.mprice_layout.setVisibility(View.VISIBLE);
            holder.button.setVisibility(View.VISIBLE);
            holder.button.setText("Check final price");

            holder.min_price.setText(Constants.priceFormat.format(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getMinquote()));
            holder.max_price.setText(Constants.priceFormat.format(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getMaxquote()));
            holder.final_price.setText("?");

        } else if (inboxArrayList.get(position).getRequeststatus().equals("Accepted Final Price")) {

            holder.mbids_count_layout.setVisibility(View.GONE);
            holder.mprice_layout.setVisibility(View.VISIBLE);
            holder.button.setVisibility(View.GONE);

            holder.min_price.setText(Constants.priceFormat.format(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getMinquote()));
            holder.max_price.setText(Constants.priceFormat.format(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getMaxquote()));
            holder.final_price.setText(Constants.priceFormat.format(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getFinalprice()));

        } else if (inboxArrayList.get(position).getRequeststatus().equals("Pending Accept Final Price")) {

            holder.mbids_count_layout.setVisibility(View.GONE);
            holder.mprice_layout.setVisibility(View.VISIBLE);
            holder.button.setVisibility(View.VISIBLE);
            holder.button.setText("Accept final price");

            holder.min_price.setText(Constants.priceFormat.format(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getMinquote()));
            holder.max_price.setText(Constants.priceFormat.format(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getMaxquote()));
            holder.final_price.setText(Constants.priceFormat.format(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getFinalprice()));
        }

        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(inboxArrayList.get(position).getRequeststatus().equalsIgnoreCase("pending")){
                    Intent intent = new Intent(context, OffersActivity.class);
                    intent.putExtra("id", ""+inboxArrayList.get(position).getRequestid());
                    intent.putExtra("carData", inboxArrayList);
                    intent.putExtra("carPos",position);
                    context.startActivity(intent);
                }
                else if(inboxArrayList.get(position).getRequeststatus().equalsIgnoreCase("Accepted Final Price")){

                }
                else {
                    Intent intent = new Intent(context, FinalPrice_Bid.class);
                    intent.putExtra("data", inboxArrayList);
                    intent.putExtra("bidPos",position);
                    context.startActivity(intent);
                }
            }
        });

        holder.mbids_count_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OffersActivity.class);
                intent.putExtra("id", ""+inboxArrayList.get(position).getRequestid());
                intent.putExtra("carData", inboxArrayList);
                intent.putExtra("carPos",position);
                context.startActivity(intent);
            }
        });
        return convertView;
    }

}

