package com.cs.samkracustomer.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.cs.samkracustomer.Activity.VehicleActivity;
import com.cs.samkracustomer.Models.GetUserNewRequestResponse;
import com.cs.samkracustomer.Models.OffersResponse;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Utils.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class OffersAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<OffersResponse.Data> historyArrayList = new ArrayList<>();
    String language;

    public OffersAdapter(Context context, ArrayList<OffersResponse.Data> orderList, String language) {
        this.context = context;
        this.historyArrayList = orderList;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return historyArrayList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView requestCode, distance, rating, minQuote, maxQuote;
        RatingBar ratingBar;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")) {
            convertView = inflater.inflate(R.layout.receiving_child, null);
//            }
//            else{
//                convertView = inflater.inflate(R.layout.list_host_history_ar, null);
//            }

            holder.requestCode = (TextView) convertView.findViewById(R.id.request_code);
            holder.distance = (TextView) convertView.findViewById(R.id.distance);
            holder.rating = (TextView) convertView.findViewById(R.id.rating);
            holder.minQuote = (TextView) convertView.findViewById(R.id.minumum_quote);
            holder.maxQuote = (TextView) convertView.findViewById(R.id.maxumum_quote);

            holder.ratingBar = (RatingBar) convertView.findViewById(R.id.ratingbar);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.requestCode.setText(""+historyArrayList.get(position).getRequestCode());
        holder.minQuote.setText(Constants.priceFormat.format(historyArrayList.get(position).getMinquote()));
        holder.maxQuote.setText(Constants.priceFormat.format(historyArrayList.get(position).getMaxquote()));
        holder.distance.setText(""+historyArrayList.get(position).getWk().get(0).getWorkshopdistance() + "KM");

        if(historyArrayList.get(position).getWk().get(0).getAveragerating() == 0){
            holder.rating.setText("5/5");
            holder.ratingBar.setRating(5);
        }
        else{
            holder.rating.setText(historyArrayList.get(position).getWk().get(0).getAveragerating()+"/5");
            holder.ratingBar.setRating(historyArrayList.get(position).getWk().get(0).getAveragerating());
        }


        return convertView;
    }




}