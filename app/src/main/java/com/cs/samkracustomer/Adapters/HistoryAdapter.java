package com.cs.samkracustomer.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.samkracustomer.Models.AccpetedOffersResponce;
import com.cs.samkracustomer.Models.OffersResponse;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Utils.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class HistoryAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<AccpetedOffersResponce.Data> historyArrayList = new ArrayList<>();
    String language;

    public HistoryAdapter(Context context, ArrayList<AccpetedOffersResponce.Data> orderList, String language) {
        this.context = context;
        this.historyArrayList = orderList;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return historyArrayList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView carname,carmodel,requstid,time,date,price;
        ImageView caricon;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")) {
            convertView = inflater.inflate(R.layout.history_child, null);
//            }
//            else{
//                convertView = inflater.inflate(R.layout.list_host_history_ar, null);
//            }

            holder.carname = (TextView) convertView.findViewById(R.id.hy_carname);
            holder.carmodel = (TextView) convertView.findViewById(R.id.hy_model);
            holder.requstid = (TextView) convertView.findViewById(R.id.hy_workshopid);
            holder.time = (TextView) convertView.findViewById(R.id.hy_time);
            holder.date = (TextView) convertView.findViewById(R.id.hy_date);
            holder.price = (TextView) convertView.findViewById(R.id.hy_price);
            holder.caricon=(ImageView) convertView.findViewById(R.id.hy_caricon);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.carname.setText(""+historyArrayList.get(position).getWs().get(0).getUsr().get(0).getCm().get(0).getCarmakernameen());
        holder.carmodel.setText(""+historyArrayList.get(position).getWs().get(0).getUsr().get(0).getCm().get(0).getMdl().get(0).getModelnameen());
        holder.requstid.setText(""+historyArrayList.get(position).getRequestcode());
        holder.price.setText(Constants.priceFormat.format(historyArrayList.get(position).getNetamount()));

        Glide.with(context)
                .load(Constants.IMAGE_URL+historyArrayList.get(position).getWs().get(0).getUsr().get(0).getCm().get(0)
                .getMdl().get(0).getRci().get(0).getDocumentlocation())
                .into(holder.caricon);

        String str = historyArrayList.get(position).getCreatedon();
        String[] array = str.split(" ");

        holder.date.setText(array[0]);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm a");

        String tt = array[1];
        try {
            Date time = sdf.parse(tt);
            tt = sdf1.format(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.time.setText(tt);

        return convertView;
    }



}