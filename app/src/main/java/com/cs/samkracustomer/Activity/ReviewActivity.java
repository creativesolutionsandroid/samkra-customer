package com.cs.samkracustomer.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkracustomer.Dialogs.VerifyOtpDialog;
import com.cs.samkracustomer.Models.NotificationResponce;
import com.cs.samkracustomer.Models.ReviewResponse;
import com.cs.samkracustomer.Models.SaveuserRatingResponce;
import com.cs.samkracustomer.Models.VerifyMobileResponse;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewActivity extends AppCompatActivity {

    final Context context = this;
    TextView workshopid, km, feedback;
    RatingBar ratingbar;
    EditText suggestions;
    Button nothanks, submit;
    ArrayList<NotificationResponce.Data> data = new ArrayList<>();
    String Language = "En";
    String TAG = "TAG";
    String str;
    float givenRating = 1;

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String userId;
    ImageView backBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.review);

        workshopid = (TextView) findViewById(R.id.tv_shopid);
        km = (TextView) findViewById(R.id.tv_km);
        feedback = (TextView) findViewById(R.id.tv_feedback);
        ratingbar = (RatingBar) findViewById(R.id.ratingbar);
        suggestions = (EditText) findViewById(R.id.ed_command);
        nothanks = (Button) findViewById(R.id.btn_nothanks);
        submit = (Button) findViewById(R.id.btn_submit);
        backBtn = (ImageView) findViewById(R.id.back_btn);

        workshopid.setText(getIntent().getStringExtra("rCode"));
        km.setText(getIntent().getStringExtra("distance"));
        ratingbar.setRating(getIntent().getFloatExtra("rating", 1));
        setComments(getIntent().getFloatExtra("rating", 1));

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        suggestions.setImeOptions(EditorInfo.IME_ACTION_DONE);
        suggestions.setRawInputType(InputType.TYPE_CLASS_TEXT);

        ratingbar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                if(b){
                    if(v == 0){
                        ratingBar.setRating(1);
                    }
                    setComments(v);
                }
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        nothanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(ReviewActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new RatingRequst().execute();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    private void setComments(float rating){
        givenRating = rating;
        if(rating == 1){
            feedback.setText("Horrible");
        }
        else if(rating == 2){
            feedback.setText("Bad");
        }
        else if(rating == 3){
            feedback.setText("Good");
        }
        else if(rating == 4){
            feedback.setText("Best");
        }
        else if(rating == 5){
            feedback.setText("Super");
        }

    }


    private boolean validations() {
        str = suggestions.getText().toString();
        if (str.length() == 0) {
            suggestions.setError(getResources().getString(R.string.enter_suggestion));
            return false;
        }
        return true;
    }

    private class RatingRequst extends AsyncTask<String, String, String> {

        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            dialog = new ACProgressFlower.Builder(ReviewActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(ReviewActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ReviewResponse> call = apiService.RatingRequst(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ReviewResponse>() {
                @Override
                public void onResponse(Call<ReviewResponse> call, Response<ReviewResponse> response) {
                    if (response.isSuccessful()) {
                        ReviewResponse SaveuserRatingResponce = response.body();
                        try {
                            if (SaveuserRatingResponce.getStatus()) {
                                setResult(RESULT_OK);
                                finish();
                            } else {
                                //                          status false case
                                String failureResponse = SaveuserRatingResponce.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), ReviewActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(ReviewActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(ReviewActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ReviewResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(ReviewActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ReviewActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private String prepareVerifyMobileJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("WorkshopId", getIntent().getIntExtra("wId", 0));
            parentObj.put("RequestId", getIntent().getIntExtra("rId", 0));
            parentObj.put("UserName", userPrefs.getString("name", null));
            parentObj.put("Rating", givenRating);
            parentObj.put("Comments", str);
            parentObj.put("UserId", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareVerifyMobileJson: "+parentObj);
        return parentObj.toString();
    }
}