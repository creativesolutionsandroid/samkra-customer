package com.cs.samkracustomer.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.cs.samkracustomer.Fragements.HomeFragment;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.SideMenuAdapter;


public class MainActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    LinearLayout mDrawerLinear;
    String[] sideMenuItems;
    Integer[] sideMenuImages;
    ListView sideMenuListView;
    SideMenuAdapter mSideMenuAdapter;
    int itemSelectedPostion = 0;
    public static DrawerLayout drawer;
    FragmentManager fragmentManager = getSupportFragmentManager();

    ImageView closeMenu, logout;
    TextView userName;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLinear = (LinearLayout) findViewById(R.id.left_drawer);
        closeMenu = (ImageView) findViewById(R.id.close_btn);
        logout = (ImageView) findViewById(R.id.logout_btn);
        userName = (TextView) findViewById(R.id.username);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        HomeFragment homeScreenFragment = new HomeFragment();
        fragmentTransaction.add(R.id.fragment_layout, homeScreenFragment);
        fragmentTransaction.commit();

        sideMenuItems = new String[]{getResources().getString(R.string.menu_home),
                getResources().getString(R.string.menu_history),
                getResources().getString(R.string.menu_notification),
                getResources().getString(R.string.menu_more),
                getResources().getString(R.string.menu_morescreen)
        };

        sideMenuImages = new Integer[] {R.drawable.home_icon,
                R.drawable.menu_invoice,
                R.drawable.discoun,
                R.drawable.add_more,
                R.drawable.seetings_icon};

        sideMenuListView = (ListView) findViewById(R.id.side_menu_list_view);
        mSideMenuAdapter = new SideMenuAdapter(MainActivity.this, R.layout.list_side_menu, sideMenuItems, sideMenuImages, itemSelectedPostion);
        sideMenuListView.setAdapter(mSideMenuAdapter);
        sideMenuListView.setOnItemClickListener(new DrawerItemClickListener());

        closeMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userPrefsEditor.clear();
                userPrefsEditor.commit();
                startActivity(new Intent(MainActivity.this, SignInActivity.class));
                finish();
            }
        });
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener{

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
            itemSelectedPostion = position;
            if(position != 4 && position != 6 && position !=7 && position !=8) {
                mSideMenuAdapter = new SideMenuAdapter(MainActivity.this, R.layout.list_side_menu, sideMenuItems, sideMenuImages, itemSelectedPostion);
                sideMenuListView.setAdapter(mSideMenuAdapter);
                mSideMenuAdapter.notifyDataSetChanged();
            }
        }
    }

    public void selectItem(int position){
        switch (position){
            case 0:
                Fragment mainFragment = new HomeFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, mainFragment).commit();
                drawer.closeDrawer(GravityCompat.START);
                break;

            case 1:
                startActivity(new Intent(MainActivity.this, HistoryActivity.class));
                drawer.closeDrawer(GravityCompat.START);
                break;

            case 2:
              startActivity(new Intent(MainActivity.this, NotificationActivity.class));
               drawer.closeDrawer(GravityCompat.START);
                break;

            case 3:
                startActivity(new Intent(MainActivity.this, MyProfileActivity.class));
                drawer.closeDrawer(GravityCompat.START);
                break;
            case 4:
                startActivity(new Intent(MainActivity.this, MoreActivity.class));
                drawer.closeDrawer(GravityCompat.START);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        userName.setText(userPrefs.getString("name",""));
    }
}
