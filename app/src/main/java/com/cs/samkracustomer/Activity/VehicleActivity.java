package com.cs.samkracustomer.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkracustomer.Models.Carmakersmodelresponse;
import com.cs.samkracustomer.Models.SaveUserRequestResponse;
import com.cs.samkracustomer.Models.VerifyMobileResponse;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.GPSTracker;
import com.cs.samkracustomer.Utils.NetworkUtil;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VehicleActivity extends AppCompatActivity {
    private static final int PLACE_PICKER_REQUEST = 1;
    final Context context = this;
    private Button request_btn;
    String str;
    String strcommandview, strlocationview;
    EditText locationedittex,commandview;
    Spinner carlist,yearlist,typelist;
    TextView maketext1,typetex1,yeartext1,hondavx,yearview,summerytex;
    ImageView locationview,back_btn;
    String leftImages, rightImages, backImages, frontImages;
    ImageView imageView;
    Bitmap logo;


    private ArrayAdapter<String> cityAdapter, typeAdapter,yearAdapter;
    String strYear = "", strComment = "", strAddress;
    Double latitude, longitude;
    int makerId = 0, modelId =  0;
    int modelSelected = 0, classSelected = 0;

    ArrayList<Carmakersmodelresponse.Data> data = new ArrayList<>();
    ArrayList<String> cityArray = new ArrayList<>();
    ArrayList<String> typeArray = new ArrayList<>();
    String[] years = {"","2018","2017","2016","2015","2014","2013","2012","2011","2010","2009","2008","2007","2006","2005","2004","2003","2002","2001","2000",};

    String TAG = "TAG";
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String userId;
    GPSTracker gps;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int LOCATION_REQUEST = 3;
    private FusedLocationProviderClient mFusedLocationClient;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_info);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        leftImages = getIntent().getStringExtra("left");
        rightImages = getIntent().getStringExtra("right");
        backImages = getIntent().getStringExtra("back");
        frontImages = getIntent().getStringExtra("front");
        logo = CamraActivity.firstImage;

        imageView = (ImageView) findViewById(R.id.image);
        maketext1 = (TextView) findViewById(R.id.maketext1);
        yeartext1 = (TextView) findViewById(R.id.yeartex1);
        typetex1 = (TextView) findViewById(R.id.typetxt1);
        hondavx = (TextView) findViewById(R.id.hondavx);
        yearview = (TextView) findViewById(R.id.yearview);
        summerytex = (TextView) findViewById(R.id.summerytex);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        locationview = (ImageView) findViewById(R.id.locationview);
        locationedittex = (EditText) findViewById(R.id.locationeditex);
        commandview = (EditText) findViewById(R.id.commadview);
        request_btn = (Button) findViewById(R.id.Request_btn);

        carlist = (Spinner) findViewById(R.id.carslist);
        yearlist = (Spinner) findViewById(R.id.yearlist);
        typelist = (Spinner) findViewById(R.id.typelist);

        imageView.setImageBitmap(logo);
        commandview.setImeOptions(EditorInfo.IME_ACTION_DONE);
        commandview.setRawInputType(InputType.TYPE_CLASS_TEXT);

        request_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(VehicleActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new SaveUserRequestApi().execute();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        String networkStatus = NetworkUtil.getConnectivityStatusString(VehicleActivity.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new CarmakersmodelApi().execute();
        } else {
            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
        }


        locationview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    PlacePicker.IntentBuilder intentBuilder =
                            new PlacePicker.IntentBuilder();
//                    intentBuilder.setLatLngBounds(BOUNDS_MOUNTAIN_VIEW);
                    Intent intent = intentBuilder.build(VehicleActivity.this);
                    startActivityForResult(intent, PLACE_PICKER_REQUEST);

                } catch (GooglePlayServicesRepairableException
                        | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        commandview.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                summerytex.setText(s.toString());
                strComment = s.toString();
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(VehicleActivity.this);
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            } else {
                gps = new GPSTracker(VehicleActivity.this);
                try {
                    getGPSCoordinates();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            gps = new GPSTracker(VehicleActivity.this);
            try {
                getGPSCoordinates();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void getGPSCoordinates() throws IOException {

        if(gps != null){
            if (gps.canGetLocation()) {

                latitude = gps.getLatitude();
                longitude = gps.getLongitude();

                try {
                    mFusedLocationClient.getLastLocation().addOnFailureListener(VehicleActivity.this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.i("TAG","fused lat null");
                        }
                    })
                            .addOnSuccessListener(VehicleActivity.this, new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {
                                    // Got last known location. In some rare situations this can be null.
                                    if (location != null) {
                                        // Logic to handle location object
                                        latitude = location.getLatitude();
                                        longitude = location.getLongitude();
                                        Log.i("TAG","fused lat "+location.getLatitude());
                                        Log.i("TAG","fused long "+location.getLongitude());
                                    }
                                    else{
                                        Log.i("TAG","fused lat null");
                                    }
                                }

                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }

                String address = getAddress();
                locationedittex.setText(address);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    gps = new GPSTracker(VehicleActivity.this);
                    try {
                        getGPSCoordinates();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    Toast.makeText(VehicleActivity.this, "Location permission denied, Unable to show nearby resorts", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(VehicleActivity.this, perm));
    }
    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode, Intent data) {



        if (requestCode == PLACE_PICKER_REQUEST
                && resultCode == Activity.RESULT_OK) {

            final Place place = PlacePicker.getPlace(this, data);
            final CharSequence name = place.getName();
            final CharSequence address = place.getAddress();
            String attributions = (String) place.getAttributions();
            if (attributions == null) {
                attributions = "";
            }


            if ("".equals(place.getAddress())) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(VehicleActivity.this);

//                    if(language.equalsIgnoreCase("En")) {
                // set title
                alertDialogBuilder.setTitle("Samkra");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Sorry! we couldn't detect your location. Please place the pin on your exact location.")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
//                Toast.makeText(AddressActivity.this, "Please select a address", Toast.LENGTH_SHORT).show();
            } else {
                locationedittex.setText(place.getAddress());
                strAddress = place.getAddress().toString();
                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;
            }
        }

    }

    private boolean validations(){
        if (strComment.length() == 0){
            commandview.setError(getResources().getString(R.string.enter_command));
            Constants.requestEditTextFocus(commandview, VehicleActivity.this);
            return false;
        }
        else if(classSelected == 0 || modelSelected == 0 || strYear.equals("")){
            Constants.showOneButtonAlertDialog("Please select car details", getResources().getString(R.string.error),
                    getResources().getString(R.string.ok), VehicleActivity.this);
            return false;
        }
        else if (strAddress.equals("")){
            Constants.showOneButtonAlertDialog("Please select address", getResources().getString(R.string.error),
                    getResources().getString(R.string.ok), VehicleActivity.this);
            return false;

        }

        return true;
    }

    private class CarmakersmodelApi extends AsyncTask<String, String, String> {

        ACProgressFlower dialog;
        String inputStr;
        String TAG = "TAG";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ACProgressFlower.Builder(VehicleActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(VehicleActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<Carmakersmodelresponse> call = apiService.Carmakers();
            call.enqueue(new Callback<Carmakersmodelresponse>() {
                @Override
                public void onResponse(Call<Carmakersmodelresponse> call, Response<Carmakersmodelresponse> response) {
                    if (response.isSuccessful()) {
                        data = response.body().getData();
                        setSpinner();
                    } else {
                        Toast.makeText(VehicleActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<Carmakersmodelresponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(VehicleActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(VehicleActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private void setSpinner(){
        cityArray.add("");
        for (int i = 0; i < data.size(); i++){
            cityArray.add(data.get(i).getCarmakernameEn());
        }

        modelSelected = 0;
        typeArray.add("");
        for (int j = 0; j < data.get(modelSelected).getMdl().size(); j++) {
            typeArray.add(data.get(modelSelected).getMdl().get(j).getModelnameEn());
        }

        cityAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner, cityArray) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                return v;
            }
        };
        typeAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner,typeArray) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                return v;
            }
        };
        yearAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner, years) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                return v;
            }
        };
        carlist.setAdapter(cityAdapter);
        typelist.setAdapter(typeAdapter);
        yearlist.setAdapter(yearAdapter);

        carlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                maketext1.setText(cityArray.get(i));
                if(cityArray.get(i).equalsIgnoreCase("")) {
                    modelSelected = i;
                    makerId = data.get(i).getCarmakerid();
                    typeArray.clear();
                    typetex1.setText("");
                    hondavx.setText("");
                }
                else{
                    typetex1.setText("");
                    hondavx.setText("");
                    modelSelected = i;
                    makerId = data.get(i-1).getCarmakerid();
                    typeArray.clear();
                    typeArray.add("");
                    for (int j = 0; j < data.get((modelSelected-1)).getMdl().size(); j++) {
                        typeArray.add(data.get((modelSelected-1)).getMdl().get(j).getModelnameEn());
                    }
                }
                typelist.setAdapter(typeAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        typelist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(typeArray.get(i).equalsIgnoreCase("")){

                }
                else {
                    classSelected = i;
                    modelId = data.get((modelSelected-1)).getMdl().get((i - 1)).getModelid();
                    typetex1.setText(typeArray.get((i)));
                    hondavx.setText(typeArray.get((i)));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        yearlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                yeartext1.setText(years[i]);
                yearview.setText(years[i]+" Model");
                strYear = ""+years[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private String prepareJson(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("UserId", userId);
            parentObj.put("MakerId", makerId);
            parentObj.put("ModelId", modelId);
            parentObj.put("Year", strYear);
            parentObj.put("Address", strAddress);
            parentObj.put("Latitude", latitude);
            parentObj.put("Longitude", longitude);
            parentObj.put("UserComments", strComment);
            parentObj.put("RightImages", rightImages);
            parentObj.put("FrontImages", frontImages);
            parentObj.put("LeftImages", leftImages);
            parentObj.put("BackImages", backImages);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "Json: "+parentObj.toString());
        return parentObj.toString();
    }

    private class SaveUserRequestApi extends AsyncTask<String, Integer, String> {

        ACProgressFlower dialog;
        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson();
            dialog = new ACProgressFlower.Builder(VehicleActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(VehicleActivity.this);
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<SaveUserRequestResponse> call = apiService.saveUserRequest(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<SaveUserRequestResponse>() {
                @Override
                public void onResponse(Call<SaveUserRequestResponse> call, Response<SaveUserRequestResponse> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if (response.isSuccessful()) {
                        SaveUserRequestResponse saveUserRequestResponse = response.body();
                        try {
                            if(saveUserRequestResponse.getStatus()){
                                //status true case
                                showCompletedDialog();
                            }
                            else {
                                //status false case
                                String failureResponse = saveUserRequestResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), VehicleActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(VehicleActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(VehicleActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<SaveUserRequestResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(VehicleActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(VehicleActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if(dialog != null){
                        dialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private void showCompletedDialog(){
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.dialog_submit_;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        ImageView cancel = (ImageView) dialogView.findViewById(R.id.cancle);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VehicleActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.75;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
    public String getAddress() throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        String address = null;
        try {
            address = addresses.get(0).getAddressLine(0);
            strAddress = address;
        } catch (Exception e) {
            e.printStackTrace();
            strAddress = "";
        }

        return address;
    }
}
