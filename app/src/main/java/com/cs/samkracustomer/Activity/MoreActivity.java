package com.cs.samkracustomer.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.cs.samkracustomer.R;

import java.util.List;

public class MoreActivity extends Activity {

    TextView emailId, privacyPolicy, description;
    ImageView backBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_more);

        emailId = (TextView) findViewById(R.id.email_id);
        privacyPolicy = (TextView) findViewById(R.id.privacy_policy);
        description = (TextView) findViewById(R.id.description);
        backBtn = (ImageView) findViewById(R.id.back_btn);

        String desc = "<p><span style=\"font-size: 12.0pt; font-family: 'Calibri','sans-serif'; color: #b8bab9;\">Stay tuned with <strong>SAMKRA</strong> to find the best offers for car repair services. Our offers are time limited so make sure to use them as they appear</span><span style=\"font-size: 12.0pt; font-family: 'Calibri','sans-serif';\">.<span style=\"color: #2a44a1;\"> <a href=\"http://www.google.com/\">Read More</a></span></span></p>";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            description.setText(Html.fromHtml(desc, Html.FROM_HTML_MODE_COMPACT));
        } else {
            description.setText(Html.fromHtml(desc));
        }
        description.setMovementMethod(LinkMovementMethod.getInstance());

        emailId.setPaintFlags(emailId.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        privacyPolicy.setPaintFlags(privacyPolicy.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        emailId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("plain/text");
                    i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"info@creative-sols.com"});
                    i.putExtra(Intent.EXTRA_SUBJECT, "Samkra workshop Feedback");
                    i.putExtra(Intent.EXTRA_TITLE  , "Samkra workshop Feedback");
                    final PackageManager pm = getPackageManager();
                    final List<ResolveInfo> matches = pm.queryIntentActivities(i, 0);
                    String className = null;
                    for (final ResolveInfo info : matches) {
                        if (info.activityInfo.packageName.equals("com.google.android.gm")) {
                            className = info.activityInfo.name;

                            if(className != null && !className.isEmpty()){
                                break;
                            }
                        }
                    }
                    i.setClassName("com.google.android.gm", className);
                    try {
                        startActivity(Intent.createChooser(i, "Send mail..."));
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(MoreActivity.this, "There are no email apps installed.", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(MoreActivity.this, "There are no email apps installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
