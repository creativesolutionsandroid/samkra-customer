package com.cs.samkracustomer.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.cs.samkracustomer.Adapters.ImagesUploadedAdapter;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

public class CamraActivity extends AppCompatActivity {

    private static final int CAMERA_REQUEST = 1888;

    public static ImageView imageView;
    public static Bitmap selectedBitmap;
    private LinearLayout deleteLayout;

    LinearLayout frontLayout, backLayout, rightLayout, leftLayout;
    ImageView frontSide, backSide, rightSide, leftSide, back_btn;
    LinearLayout galleryLayout;
    ImageView captureIcon, delet_btn, nexticon;
    ViewPager viewPager;
    ImagesUploadedAdapter mAdapter;

    Boolean isImageUploaded = false;
    int sideSelected = 2;
    ArrayList<Bitmap> leftImagesSelected = new ArrayList<>();
    ArrayList<Bitmap> rightImagesSelected = new ArrayList<>();
    ArrayList<Bitmap> frontImagesSelected = new ArrayList<>();
    ArrayList<Bitmap> backImagesSelected = new ArrayList<>();
    Bitmap thumbnail;
    String imageResponse;
    private DefaultHttpClient mHttpClient11;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String userId;
    public static Bitmap firstImage = null;
    String leftImages = "", rightImages = "", frontImages = "", backImages = "";
    boolean isCamera = false;

    private static final int PICK_IMAGE_FROM_GALLERY = 2;
    private static final int STORAGE_REQUEST = 5;
    private static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final String[] CAMERA_PERMS = {
            Manifest.permission.CAMERA
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camara);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        deleteLayout = (LinearLayout) findViewById(R.id.delete_layout);
        frontLayout = (LinearLayout) findViewById(R.id.front_layout);
        backLayout = (LinearLayout) findViewById(R.id.back_layout);
        rightLayout = (LinearLayout) findViewById(R.id.right_layout);
        leftLayout = (LinearLayout) findViewById(R.id.left_layout);
        viewPager = (ViewPager) findViewById(R.id.view_pager);

        delet_btn = (ImageView) findViewById(R.id.delet_btn);
        nexticon = (ImageView) findViewById(R.id.nexticon);
//        addPhoto = (ImageView) findViewById(R.id.add_photo);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        frontSide = (ImageView) findViewById(R.id.front_side);
        backSide = (ImageView) findViewById(R.id.back_side);
        leftSide = (ImageView) findViewById(R.id.left_side);
        rightSide = (ImageView) findViewById(R.id.right_side);

        imageView = (ImageView) this.findViewById(R.id.piceview);
        captureIcon = (ImageView) this.findViewById(R.id.capture_icon);

        galleryLayout = (LinearLayout) findViewById(R.id.gallery_icon);
        captureIcon = (ImageView) findViewById(R.id.capture_icon);

        deleteLayout.setVisibility(View.INVISIBLE);
        captureIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCamera();
            }
        });

        nexticon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isImageUploaded) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(CamraActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new uploadImagesApi().execute();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_upload_images),
                            getResources().getString(R.string.error),
                            getResources().getString(R.string.ok), CamraActivity.this);
                }
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        galleryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });

        frontLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sideSelected = 3;
                setImages();
                frontSide.setImageDrawable(getResources().getDrawable(R.drawable.front_selected));
                backSide.setImageDrawable(getResources().getDrawable(R.drawable.back_car));
                leftSide.setImageDrawable(getResources().getDrawable(R.drawable.left));
                rightSide.setImageDrawable(getResources().getDrawable(R.drawable.right));
            }
        });

        backSide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sideSelected = 4;
                setImages();
                frontSide.setImageDrawable(getResources().getDrawable(R.drawable.front));
                backSide.setImageDrawable(getResources().getDrawable(R.drawable.back_selected));
                leftSide.setImageDrawable(getResources().getDrawable(R.drawable.left));
                rightSide.setImageDrawable(getResources().getDrawable(R.drawable.right));
            }
        });

        leftSide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sideSelected = 1;
                setImages();
                frontSide.setImageDrawable(getResources().getDrawable(R.drawable.front));
                backSide.setImageDrawable(getResources().getDrawable(R.drawable.back_car));
                leftSide.setImageDrawable(getResources().getDrawable(R.drawable.left_selected));
                rightSide.setImageDrawable(getResources().getDrawable(R.drawable.right));
            }
        });

        rightSide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sideSelected = 2;
                setImages();
                frontSide.setImageDrawable(getResources().getDrawable(R.drawable.front));
                backSide.setImageDrawable(getResources().getDrawable(R.drawable.back_car));
                leftSide.setImageDrawable(getResources().getDrawable(R.drawable.left));
                rightSide.setImageDrawable(getResources().getDrawable(R.drawable.right_selected));
            }
        });

        deleteLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sideSelected == 1){
                    leftImagesSelected.remove(selectedBitmap);
                }
                else if(sideSelected == 2){
                    rightImagesSelected.remove(selectedBitmap);
                }
                else if(sideSelected == 3){
                    frontImagesSelected.remove(selectedBitmap);
                }
                else if(sideSelected == 4){
                    backImagesSelected.remove(selectedBitmap);
                }
                setImages();
            }
        });
    }


    private boolean canAccessStorage() {
        return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean canAccessCamera() {
        return (hasPermission1(Manifest.permission.CAMERA));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(CamraActivity.this, perm));
    }

    public void openGallery() {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {

            if (!canAccessStorage()) {
                requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                isCamera=false;
            } else {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
            }
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case STORAGE_REQUEST:

                if (canAccessStorage()) {
                    if(isCamera){
                        openCamera();
                    }
                    else {
                        openGallery();
                    }
                }
                else {
                    Toast.makeText(CamraActivity.this, "Storage permission denied, Unable to select pictures", Toast.LENGTH_LONG).show();
                }
                break;

            case CAMERA_REQUEST:
                if (!canAccessStorage()) {
                    requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                }
                else {
                    openCamera();
                }
                break;
        }
    }

    public void openCamera() {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {

            if (!canAccessCamera()) {
                isCamera=true;
                requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
            } else {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        } else {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            thumbnail = (Bitmap) data.getExtras().get("data");
            convertPixel();
        }

        if (requestCode == PICK_IMAGE_FROM_GALLERY && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            try {
                thumbnail = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
            } catch (Exception e) {
                e.printStackTrace();
            }
            convertPixel();
        }

    }

    private void convertPixel() {
        thumbnail = Bitmap.createScaledBitmap(thumbnail, 900, 800,
                false);
        thumbnail = codec(thumbnail, Bitmap.CompressFormat.JPEG, 100);
        Log.d("TAG", "convertPixel: "+thumbnail.getByteCount());
        imageView.setImageBitmap(thumbnail);
        selectedBitmap = thumbnail;
        if(sideSelected == 1) {
            leftImagesSelected.add(thumbnail);
        }
        else if(sideSelected == 2) {
            rightImagesSelected.add(thumbnail);
        }
        else if(sideSelected == 3) {
            frontImagesSelected.add(thumbnail);
        }
        else if(sideSelected == 4) {
            backImagesSelected.add(thumbnail);
        }
        setImages();
    }

    private Bitmap codec(Bitmap src, Bitmap.CompressFormat format, int quality) {

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        src.compress(format, quality, os);

        byte[] array = os.toByteArray();
        return BitmapFactory.decodeByteArray(array, 0, array.length);

    }

    private void setImages() {
        ArrayList<Bitmap> imagesSelected = new ArrayList<>();

        if(sideSelected == 1) {
            imagesSelected.addAll(leftImagesSelected);
        }
        else if(sideSelected == 2) {
            imagesSelected.addAll(rightImagesSelected);
        }
        else if(sideSelected == 3) {
            imagesSelected.addAll(frontImagesSelected);
        }
        else if(sideSelected == 4) {
            imagesSelected.addAll(backImagesSelected);
        }

        mAdapter = new ImagesUploadedAdapter(CamraActivity.this, imagesSelected);
        viewPager.setAdapter(mAdapter);
        if(imagesSelected.size()>0){
            isImageUploaded = true;
            deleteLayout.setVisibility(View.VISIBLE);
            imageView.setImageBitmap(imagesSelected.get(0));
        }
        else{
            deleteLayout.setVisibility(View.INVISIBLE);
            imageView.setImageBitmap(null);
        }
    }

    private class uploadImagesApi extends AsyncTask<String, String, String> {

        ACProgressFlower dialog;
        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ACProgressFlower.Builder(CamraActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... str) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(CamraActivity.this);

            HttpResponse httpResponse = null;
            int count = 0, usedCount = 0;

            MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

            HttpParams params = new BasicHttpParams();
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            mHttpClient11 = new DefaultHttpClient(params);
            HttpPost httppost = new HttpPost(Constants.IMAGE_UPLOAD_URL);

            try{
                for (int i = 0; i < rightImagesSelected.size(); i++) {
                    Bitmap thumbnail1 = null;
                    String imagePath1 = "";
                    File file=null;
                    thumbnail1 = rightImagesSelected.get(i);
                    if(firstImage == null){
                        firstImage = rightImagesSelected.get(i);
                    }
                    if (thumbnail1 != null) {
                        imagePath1 = StoreByteImage(thumbnail1, "right");
                        count = count + 1;
                    }

                    if (imagePath1 != null) {
                        file = new File(imagePath1);
                    }

                    if (null != file && file.exists()) {
                        multipartEntity.addPart("image", new FileBody(file));
                    }
                }

                for (int i = 0; i < leftImagesSelected.size(); i++) {
                    Bitmap thumbnail1 = null;
                    String imagePath1 = "";
                    File file=null;
                    thumbnail1 = leftImagesSelected.get(i);
                    if(firstImage == null){
                        firstImage = leftImagesSelected.get(i);
                    }
                    if (thumbnail1 != null) {
                        imagePath1 = StoreByteImage(thumbnail1, "left");
                        count = count + 1;
                    }

                    if (imagePath1 != null) {
                        file = new File(imagePath1);
                    }

                    if (null != file && file.exists()) {
                        multipartEntity.addPart("image", new FileBody(file));
                    }
                }

                for (int i = 0; i < frontImagesSelected.size(); i++) {
                    Bitmap thumbnail1 = null;
                    String imagePath1 = "";
                    File file=null;
                    thumbnail1 = frontImagesSelected.get(i);
                    if(firstImage == null){
                        firstImage = frontImagesSelected.get(i);
                    }
                    if (thumbnail1 != null) {
                        imagePath1 = StoreByteImage(thumbnail1, "front");
                        count = count + 1;
                    }

                    if (imagePath1 != null) {
                        file = new File(imagePath1);
                    }

                    if (null != file && file.exists()) {
                        multipartEntity.addPart("image", new FileBody(file));
                    }
                }

                for (int i = 0; i < backImagesSelected.size(); i++) {
                    Bitmap thumbnail1 = null;
                    String imagePath1 = "";
                    File file=null;
                    thumbnail1 = backImagesSelected.get(i);
                    if(firstImage == null){
                        firstImage = backImagesSelected.get(i);
                    }
                    if (thumbnail1 != null) {
                        imagePath1 = StoreByteImage(thumbnail1, "back");
                        count = count + 1;
                    }

                    if (imagePath1 != null) {
                        file = new File(imagePath1);
                    }

                    if (null != file && file.exists()) {
                        multipartEntity.addPart("image", new FileBody(file));
                    }
                }

                httppost.setEntity(multipartEntity);
                httpResponse=  mHttpClient11.execute(httppost);
            }catch(Exception e){
                e.printStackTrace();
            }

            InputStream entity = null;
            try {
                entity = httpResponse.getEntity().getContent();
                if(entity != null) {
                    imageResponse = convertInputStreamToString(entity);
                    Log.i("TAG", "user response:" + imageResponse);
                }
            }catch(NullPointerException e)
            {
                e.printStackTrace();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            // TODO Auto-generated method stub
            if(imageResponse!=null && imageResponse.length()>0) {
                try {
                    JSONArray resultArray = new JSONArray(imageResponse);
                    for (int i = 0; i < resultArray.length(); i++){
                        JSONObject jsonObject = resultArray.getJSONObject(i);
                        if(!jsonObject.getString("Name").equals("")){
                            Intent intent = new Intent(CamraActivity.this, VehicleActivity.class);
//                            intent.putExtra("logo", firstImage);
                            intent.putExtra("left", leftImages);
                            intent.putExtra("right", rightImages);
                            intent.putExtra("front", frontImages);
                            intent.putExtra("back", backImages);
                            startActivity(intent);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else{
//                if(language.equalsIgnoreCase("En")) {
                Toast.makeText(CamraActivity.this, "Images upload failed.Please try again", Toast.LENGTH_SHORT).show();
//                }
//                else{
//                    Toast.makeText(getContext(), "خطأ في تحميل الصور ، الرجاء إعادة المحاولة", Toast.LENGTH_SHORT).show();
//                }
            }
            if(dialog!=null){
                dialog.dismiss();
            }
            super.onPostExecute(s);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    public String StoreByteImage(Bitmap bitmap, String type) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(new Date());
        String pictureName = "";
        if(type.equals("left") ) {
            pictureName = "LEFT_" + userId + "_" + timeStamp + ".jpg";
            if(leftImages.equals("")){
                leftImages = pictureName;
            }
            else {
                leftImages = leftImages + "," +pictureName;
            }
        }
        else if(type.equals("right") ) {
            pictureName = "RIGHT_" + userId + "_" + timeStamp + ".jpg";
            if(rightImages.equals("")){
                rightImages = pictureName;
            }
            else {
                rightImages = rightImages + "," +pictureName;
            }
        }
        else if(type.equals("front") ) {
            pictureName = "FRONT_" + userId + "_" + timeStamp + ".jpg";
            if(frontImages.equals("")){
                frontImages = pictureName;
            }
            else {
                frontImages = frontImages + "," +pictureName;
            }
        }
        else if(type.equals("back") ) {
            pictureName = "BACK_" + userId + "_" + timeStamp + ".jpg";
            if(backImages.equals("")){
                backImages = pictureName;
            }
            else {
                backImages = backImages + "," +pictureName;
            }
        }

        File sdImageMainDirectory = new File("/sdcard/"+ pictureName);

        FileOutputStream fileOutputStream = null;

        String nameFile = null;

        try {

            BitmapFactory.Options options=new BitmapFactory.Options();

            fileOutputStream = new FileOutputStream(sdImageMainDirectory);

            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);

            bitmap.compress(Bitmap.CompressFormat.JPEG,100, bos);

            bos.flush();

            bos.close();

        } catch (FileNotFoundException e) {

            // TODO Auto-generated catch block

            e.printStackTrace();

        } catch (IOException e) {

            // TODO Auto-generated catch block

            e.printStackTrace();
        }
        return sdImageMainDirectory.getAbsolutePath();
    }
}

