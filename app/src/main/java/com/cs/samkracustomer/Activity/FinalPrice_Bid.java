package com.cs.samkracustomer.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkracustomer.Models.AccpetedOffersResponce;
import com.cs.samkracustomer.Models.GetUserNewRequestResponse;
import com.cs.samkracustomer.Models.OffersResponse;
import com.cs.samkracustomer.Models.PlacebidResponce;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FinalPrice_Bid extends AppCompatActivity {


    ImageView backBtn,location,done,home;
    TextView requestcode,distance,carname,carclass,year;
    EditText customercomment,customerFinalComment,wrokshopcomment,minimumquote,maximumquote,wrokshopnote,finalprice;
    String TAG = "TAG";
    final Context context = this;

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String userId;
    ArrayList<GetUserNewRequestResponse.Data> bidData = new ArrayList<>();
    int bidPos;
    LinearLayout submitLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_bid);

        requestcode =(TextView)findViewById(R.id.fp_request_code);
        distance =(TextView)findViewById(R.id.fp_distance);
        carname =(TextView)findViewById(R.id.fp_car_model);
        carclass =(TextView)findViewById(R.id.fp_s_class);
        year =(TextView)findViewById(R.id.fp_year);
        customercomment=(EditText)findViewById(R.id.fp_user_comment);
        customerFinalComment = (EditText) findViewById(R.id.fp_user_final_comment);
        wrokshopcomment=(EditText)findViewById(R.id.fp_wrokshop_comment);
        minimumquote=(EditText)findViewById(R.id.fp_min_quote);
        maximumquote=(EditText)findViewById(R.id.fp_max_quote);
        wrokshopnote=(EditText)findViewById(R.id.fp_workshop_note);
        finalprice=(EditText)findViewById(R.id.fp_final_quote);
        backBtn=(ImageView)findViewById(R.id.fp_back_btn);
        home=(ImageView)findViewById(R.id.fp_home_btn);
        done=(ImageView)findViewById(R.id.fp_done_btn);
        submitLayout = (LinearLayout) findViewById(R.id.submit_layout);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        bidData = (ArrayList<GetUserNewRequestResponse.Data>) getIntent().getSerializableExtra("data");
        bidPos = getIntent().getIntExtra("bidPos", 0);

        requestcode.setText(bidData.get(bidPos).getRequestcode());
        carname.setText(bidData.get(bidPos).getCm().get(0).getCarmakernameen());
        carclass.setText(bidData.get(bidPos).getCm().get(0).getMdl().get(0).getModelnameen());
        year.setText(""+bidData.get(bidPos).getYear());
        customercomment.setText(bidData.get(bidPos).getUsercomments());
        wrokshopcomment.setText(bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getWorkshopcomments());
        distance.setText(""+bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getWorkshopdistance()+"KM");
        minimumquote.setText(Constants.priceFormat.format(bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getMinquote()));
        maximumquote.setText(Constants.priceFormat.format(bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getMaxquote()));
        wrokshopnote.setText(bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getFinalPriceWSComment());
        customerFinalComment.setText(bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getUserAcceptedComment());
        finalprice.setText(Constants.priceFormat.format(bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getFinalprice()));

        Log.d(TAG, "onCreate: "+bidData.get(bidPos).getRequeststatus());
        if(bidData.get(bidPos).getRequeststatus().equalsIgnoreCase("Pending Accept Final Price")){
            submitLayout.setVisibility(View.VISIBLE);
        }

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String networkStatus = NetworkUtil.getConnectivityStatusString(FinalPrice_Bid.this);
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    new userAccpet().execute();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }
            }

        });
    }

    private class  userAccpet extends AsyncTask<String, String, String> {

        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareLoginJson();
            dialog = new ACProgressFlower.Builder(FinalPrice_Bid.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(FinalPrice_Bid.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<PlacebidResponce> call = apiService.AcceptUserFinalBidPrice(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<PlacebidResponce>() {
                @Override
                public void onResponse(Call<PlacebidResponce> call, Response<PlacebidResponce> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if (response.isSuccessful()) {
                        PlacebidResponce PlacebidResponce = response.body();
                        try {
                            if (PlacebidResponce.getStatus()) {
                                showCompletedDialog ();
                            } else {
                                //                          status false case
                                String failureResponse = PlacebidResponce.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), FinalPrice_Bid.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(FinalPrice_Bid.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(FinalPrice_Bid.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<PlacebidResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    } else {
                        Toast.makeText(FinalPrice_Bid.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            return null;
        }

    }

    private void showCompletedDialog () {
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.dialog_thankyou;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        ImageView cancel = (ImageView) dialogView.findViewById(R.id.ty_cancle);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FinalPrice_Bid.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.65;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
    private String prepareLoginJson(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("BidId",bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getBidid());
            parentObj.put("UserId",userId);
            parentObj.put("RequestId",bidData.get(bidPos).getRequestid());
            parentObj.put("WorkshopId",bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getWorkshopid());
            Log.d(TAG, "prepareLoginJson: "+parentObj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return parentObj.toString();
    }

}