package com.cs.samkracustomer.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkracustomer.Adapters.HistoryAdapter;
import com.cs.samkracustomer.Adapters.NewUserRequestsAdapter;
import com.cs.samkracustomer.Models.AccpetedOffersResponce;
import com.cs.samkracustomer.Models.GetUserNewRequestResponse;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryActivity extends AppCompatActivity {


    ImageView back_btn;
//    TextView invoicenumber,offernumber;
    ListView requestsList;
    HistoryAdapter mAdapter;
    ArrayList<AccpetedOffersResponce.Data> data = new ArrayList<>();
    String Language = "En";
    String TAG = "TAG";

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String userId;
    TextView offernumber;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.historory_layout);

        back_btn=(ImageView)findViewById(R.id.hy_back_btn);
//        invoicenumber=(TextView)findViewById(R.id.hy_invoicenumber);
//        offernumber=(TextView)findViewById(R.id.hy_accpetnumber);
        requestsList=(ListView) findViewById(R.id.hy_listview);
        offernumber=(TextView)findViewById(R.id.n_bid_count);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        requestsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(HistoryActivity.this, InvoiceActivity.class);
                intent.putExtra("data", data);
                intent.putExtra("pos",i);
                startActivity(intent);
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        String networkStatus = NetworkUtil.getConnectivityStatusString(HistoryActivity.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new userRequestsApi().execute();
        }
        else{
            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
        }

    }

    private class  userRequestsApi extends AsyncTask<String, String, String> {

        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            dialog = new ACProgressFlower.Builder(HistoryActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(HistoryActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<AccpetedOffersResponce> call = apiService.historylist(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<AccpetedOffersResponce>() {
                @Override
                public void onResponse(Call<AccpetedOffersResponce> call, Response<AccpetedOffersResponce> response) {
                    if (response.isSuccessful()) {
                        Log.d(TAG, "onResponse: "+response);
                        AccpetedOffersResponce verifyMobileResponse = response.body();
                        try {
                            if (verifyMobileResponse.getStatus()) {
                                data = verifyMobileResponse.getData();
                                if(data.size() == 0){
                                    Constants.showOneButtonAlertDialog("No history found", getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), HistoryActivity.this);
                                }
                                mAdapter = new HistoryAdapter(HistoryActivity.this, data, Language);
                                requestsList.setAdapter(mAdapter);
                                offernumber.setText(""+data.size());
                            } else {
                                //                          status false case
                                String failureResponse = verifyMobileResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), HistoryActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(HistoryActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(HistoryActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
                @Override
                public void onFailure(Call<AccpetedOffersResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(HistoryActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(HistoryActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            return null;
        }
        private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("UserId", userId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }

    }

}