package com.cs.samkracustomer.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.cs.samkracustomer.Adapters.NewUserRequestsAdapter;
import com.cs.samkracustomer.Adapters.ReviewsAdapter;
import com.cs.samkracustomer.Models.GetUserNewRequestResponse;
import com.cs.samkracustomer.Models.OffersResponse;
import com.cs.samkracustomer.R;

import java.util.ArrayList;

public class ProfileActivity extends AppCompatActivity {

    ImageView backbtn;
    int bidPos;
    ArrayList<OffersResponse.Data> bidData = new ArrayList<>();

    ArrayList<OffersResponse.Wr> reviews = new ArrayList<>();
    TextView wrokshopname,wrokshopdetails,custmore_reviews;
    RatingBar ratingBar;
    ListView requestsList;
    ReviewsAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);

        wrokshopname =(TextView) findViewById(R.id.wr_workshopid);
        wrokshopdetails =(TextView) findViewById(R.id.wr_summerytex);
        custmore_reviews =(TextView) findViewById(R.id.wr_revies);
        ratingBar =(RatingBar) findViewById(R.id.ratingbar);
        backbtn =(ImageView) findViewById(R.id.pf_back);

        ArrayList<OffersResponse.Data> bidData = new ArrayList<>();
        ArrayList<GetUserNewRequestResponse.Data> carData = new ArrayList<>();
        bidData = (ArrayList<OffersResponse.Data>) getIntent().getSerializableExtra("bidData");
        bidPos = getIntent().getIntExtra("bidPos", 0);

       reviews.addAll(bidData.get(bidPos).getWk().get(0).getWr());

        wrokshopname.setText(bidData.get(bidPos).getWk().get(0).getWorkshopnameEn());
        wrokshopdetails.setText(bidData.get(bidPos).getWk().get(0).getAbouten());
        custmore_reviews.setText("("+reviews.size()+" Reviews)");
        ratingBar.setRating(bidData.get(bidPos).getWk().get(0).getAveragerating());

        requestsList = (ListView) findViewById(R.id.listview_pf);

        mAdapter = new ReviewsAdapter(ProfileActivity.this, reviews, "");
        requestsList.setAdapter(mAdapter);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



    }
}
