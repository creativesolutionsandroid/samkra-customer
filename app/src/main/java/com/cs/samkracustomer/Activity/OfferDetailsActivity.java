package com.cs.samkracustomer.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.samkracustomer.Models.GetUserNewRequestResponse;
import com.cs.samkracustomer.Models.OffersResponse;
import com.cs.samkracustomer.Models.PlacebidResponce;
import com.cs.samkracustomer.Models.VerifyMobileResponse;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OfferDetailsActivity extends Activity {

    private static final int REQUEST_PHONE_CALL = 1;
    ImageView backBtn,caricon,location,confirm;
    TextView carmodel,wrokshopid,rating,minimumquote,maxmumumquote,command,year,viewprofile, distance;
    String TAG = "TAG";
    String strComment,BidId,UserId,RequestId,WorkshopId,UserAcceptedComment;
    RatingBar ratingBar;
    ImageView bidlist,ok,phone;
    final Context context = this;
    EditText inputcomment;

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String userId;

    private static final String[] PHONE_PERMS = {
            Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;

    ArrayList<OffersResponse.Data> bidData = new ArrayList<>();
    ArrayList<GetUserNewRequestResponse.Data> carData = new ArrayList<>();
    int carPos, bidPos;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bid_details);


        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        bidData = (ArrayList<OffersResponse.Data>) getIntent().getSerializableExtra("bidData");
        carData = (ArrayList<GetUserNewRequestResponse.Data>) getIntent().getSerializableExtra("carData");
        bidPos = getIntent().getIntExtra("bidPos", 0);
        carPos = getIntent().getIntExtra("carPos", 0);

        backBtn =(ImageView) findViewById(R.id.bd_back);
//        location =(ImageView) findViewById(R.id.im_location);
        caricon =(ImageView) findViewById(R.id.im_caricon);
        carmodel =(TextView) findViewById(R.id.tv_carmodel);
        viewprofile=(TextView) findViewById(R.id.view_profile);
        wrokshopid =(TextView) findViewById(R.id.tv_shopid);
        rating =(TextView) findViewById(R.id.tv_rating);
        maxmumumquote =(TextView) findViewById(R.id.tv_mxq);
        minimumquote =(TextView) findViewById(R.id.tv_mq);
        command =(TextView) findViewById(R.id.tv_summerytex);
        year =(TextView) findViewById(R.id.yearview);
        ratingBar=(RatingBar) findViewById(R.id.ratingbar);
        bidlist=(ImageView) findViewById(R.id.bidlist);
        confirm=(ImageView) findViewById(R.id.pf_confirm_icon);
        phone=(ImageView) findViewById(R.id.phone_icon);
        confirm=(ImageView) findViewById(R.id.pf_confirm_icon);
        inputcomment=(EditText) findViewById(R.id.ed_commadview);
        distance = (TextView) findViewById(R.id.distance);

        viewprofile.setPaintFlags(viewprofile.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);

        minimumquote.setText(Constants.priceFormat.format(bidData.get(bidPos).getMinquote()));
        maxmumumquote.setText(Constants.priceFormat.format(bidData.get(bidPos).getMaxquote()));
        wrokshopid.setText(""+bidData.get(bidPos).getRequestCode());
        rating.setText(""+bidData.get(bidPos).getWk().get(0).getAveragerating());
        command.setText(bidData.get(bidPos).getWorkshopcomments());
        carmodel.setText(carData.get(carPos).getCm().get(0).getMdl().get(0).getModelnameen());
        year.setText(carData.get(carPos).getYear()+" year");
        ratingBar.setRating(bidData.get(bidPos).getWk().get(0).getAveragerating());
        try {
            distance.setText(Constants.priceFormat.format(bidData.get(bidPos).getWk().get(0).getWorkshopdistance())+" KM");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Glide.with(OfferDetailsActivity.this)
                .load(Constants.IMAGE_URL+carData.get(carPos).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(0).getDocumentlocation())
                .into(caricon);
        Intent intent = new Intent(OfferDetailsActivity.this, FinalPrice_Bid.class);
        bidlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        backBtn .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(OfferDetailsActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new userAccpet().execute();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        viewprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OfferDetailsActivity.this, ProfileActivity.class);
                i.putExtra("bidData", bidData);
                i.putExtra("bidPos",bidPos);
                startActivity(i);
            }
        });
        phone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                        }
                    } else {
                        String mobile = "+"+bidData.get(bidPos).getWk().get(0).getMobileNo();
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: "+mobile));
                        if (ActivityCompat.checkSelfPermission(OfferDetailsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                            return;
                        }
                        startActivity(intent);
                    }
                } else {
                    String mobile = "+"+bidData.get(bidPos).getWk().get(0).getMobileNo();
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: "+mobile));
                }
            }
        });
    }

    private boolean canAccessPhonecalls() {
        return (hasPermission(Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(OfferDetailsActivity.this, perm));
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: +966920000555"));
                    if (ActivityCompat.checkSelfPermission(OfferDetailsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                        return;
                    }
                    startActivity(intent);
                } else {
                    Toast.makeText(OfferDetailsActivity.this, "Call phone permission denied, Unable to make call", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    private boolean validations() {
        strComment = inputcomment.getText().toString();
        if (strComment.length() == 0) {
            inputcomment.setError(getResources().getString(R.string.enter_command));
            return false;
        }
        return true;
    }
    private class  userAccpet extends AsyncTask<String, String, String> {

        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareLoginJson();
            dialog = new ACProgressFlower.Builder(OfferDetailsActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(OfferDetailsActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<PlacebidResponce> call = apiService.userAccpet(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<PlacebidResponce>() {
                @Override
                public void onResponse(Call<PlacebidResponce> call, Response<PlacebidResponce> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if (response.isSuccessful()) {
                        PlacebidResponce PlacebidResponce = response.body();
                        try {
                            if (PlacebidResponce.getStatus()) {
                                showCompletedDialog ();
                            } else {
                                //                          status false case
                                String failureResponse = PlacebidResponce.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), OfferDetailsActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(OfferDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(OfferDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<PlacebidResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    } else {
                        Toast.makeText(OfferDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private void showCompletedDialog () {
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.dialog_thankyou;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        ImageView cancel = (ImageView) dialogView.findViewById(R.id.ty_cancle);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OfferDetailsActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.75;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
    private String prepareLoginJson(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("BidId",bidData.get(bidPos).getRbid());
            parentObj.put("UserId",userId);
            parentObj.put("RequestId",bidData.get(bidPos).getRequestid());
            parentObj.put("WorkshopId",bidData.get(bidPos).getWorkshopid());
            parentObj.put("UserAcceptedComment",strComment);
            Log.d(TAG, "prepareLoginJson: "+parentObj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return parentObj.toString();
    }
}

