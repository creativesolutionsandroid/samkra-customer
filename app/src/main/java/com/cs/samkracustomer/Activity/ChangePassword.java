package com.cs.samkracustomer.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.cs.samkracustomer.Models.Signupresponse;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChangePassword extends Activity {

    private String response12 = null;
    ProgressDialog dialog;

    EditText oldPassword, newPassword, confirmPassword;
    Button submit;
    SharedPreferences userPrefs;
    String response, userId,strPhone,strPassword,strNewpassword;
    Toolbar toolbar;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor userPrefEditor;
    String language;
    ImageView backBtn;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
            setContentView(R.layout.change_password);

            userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
            userPrefEditor = userPrefs.edit();
            userId = userPrefs.getString("userId", null);
            strPhone = userPrefs.getString("mobile", null);

            backBtn = (ImageView) findViewById(R.id.back_btn);

            oldPassword = (EditText) findViewById(R.id.old_password);
            newPassword = (EditText) findViewById(R.id.new_password);
            confirmPassword = (EditText) findViewById(R.id.confirm_password);
            submit = (Button) findViewById(R.id.change_password_button);


            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String oldPwd = oldPassword.getText().toString();
                    String newPwd = newPassword.getText().toString();
                    String confirmPwd = confirmPassword.getText().toString();
                    strPassword = oldPwd;
                    strNewpassword = newPwd;
                    if (oldPwd.length() == 0) {
                        oldPassword.setError("Please enter old password");
                    } else if (newPwd.length() == 0) {
                        newPassword.setError("Please enter new password");
                    } else if (newPwd.length() < 4) {
                        if (language.equalsIgnoreCase("En")) {
                            newPassword.setError("Password must be at least 8 characters");
                        }
                    } else if (confirmPwd.length() == 0) {
                        confirmPassword.setError("Please retype password");
                    } else if (!newPwd.equals(confirmPwd)) {
                        confirmPassword.setError("Passwords not match, please retype");
                    } else {
                        new ChangePasswordResponse().execute(userId, oldPwd, newPwd);
                    }
                }
            });

            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

    }

    private String prepareResetPasswordJson(){
        JSONObject parentObj = new JSONObject();


        try {
            parentObj.put("Phone",""+strPhone);
            parentObj.put("Password",strPassword);
            parentObj.put("NewPassword", strNewpassword);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG","prepareResetPasswordJson: "+parentObj.toString());
        return parentObj.toString();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private class ChangePasswordResponse extends AsyncTask<String, Integer, String> {

        ACProgressFlower dialog;
        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareResetPasswordJson();
            dialog = new ACProgressFlower.Builder(ChangePassword.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<Signupresponse> call = apiService.changepassword(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<Signupresponse>() {
                @Override
                public void onResponse(Call<Signupresponse> call, Response<Signupresponse> response) {
                    Log.d("TAG", "onResponse: "+response);
                    if(response.isSuccessful()){
                        Signupresponse resetPasswordResponse = response.body();
                        try {
                            if(resetPasswordResponse.getStatus()){
//                                status true case
                                String userId = resetPasswordResponse.getData().getUserid();
                                userPrefEditor.putString("userId", userId);
                                userPrefEditor.putString("name", resetPasswordResponse.getData().getUsername());
                                userPrefEditor.putString("email", resetPasswordResponse.getData().getEmail());
                                userPrefEditor.putString("mobile", resetPasswordResponse.getData().getPhone());
                                userPrefEditor.commit();
                                finish();
//                                Toast.makeText(getActivity(), R.string.reset_success_msg, Toast.LENGTH_SHORT).show();
//                                ForgotPasswordActivity.isResetSuccessful = true;
//                                getDialog().dismiss();
                            }
                            else {
//                                status false case
                                String failureResponse = resetPasswordResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), ChangePassword.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(ChangePassword.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        Toast.makeText(ChangePassword.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if(dialog != null){
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<Signupresponse> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ChangePassword.this);
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(ChangePassword.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(ChangePassword.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    if(dialog != null){
                        dialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

}
