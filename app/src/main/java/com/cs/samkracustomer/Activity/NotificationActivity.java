package com.cs.samkracustomer.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkracustomer.Adapters.HistoryAdapter;
import com.cs.samkracustomer.Adapters.NewUserRequestsAdapter;
import com.cs.samkracustomer.Adapters.NotificationAdapter;
import com.cs.samkracustomer.Models.AccpetedOffersResponce;
import com.cs.samkracustomer.Models.GetUserNewRequestResponse;
import com.cs.samkracustomer.Models.NotificationResponce;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends AppCompatActivity {

    ImageView back_btn;
    TextView offernumber;
    ListView requestsList;
    NotificationAdapter mAdapter;
    ArrayList<NotificationResponce.Data> data = new ArrayList<>();
    String Language = "En";
    String TAG = "TAG";

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String userId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_layout);

        back_btn=(ImageView)findViewById(R.id.n_back_btn);
        offernumber=(TextView)findViewById(R.id.n_bid_count);
        requestsList=(ListView) findViewById(R.id.notification_list);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);
        String networkStatus = NetworkUtil.getConnectivityStatusString(NotificationActivity.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new userRequestsApi().execute();
        }
        else{
            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
        }


//        requestsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Intent intent = new Intent(NotificationActivity.this, FinalPrice_Bid.class);
//                intent.putExtra("id", ""+data.get(i).getRequestid());
//                intent.putExtra("data", data);
//                intent.putExtra("bidPos",i);
//                startActivity(intent);
//            }
//        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    private class  userRequestsApi extends AsyncTask<String, String, String> {

        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            dialog = new ACProgressFlower.Builder(NotificationActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(NotificationActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<NotificationResponce> call = apiService.Notification(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<NotificationResponce>() {
                @Override
                public void onResponse(Call<NotificationResponce> call, Response<NotificationResponce> response) {
                    if (response.isSuccessful()) {
                        Log.d(TAG, "onResponse: "+response);
                        NotificationResponce verifyMobileResponse = response.body();
                        try {
                            if (verifyMobileResponse.getStatus()) {
                                data = verifyMobileResponse.getData();
                                if(data.size() == 0){
                                    Constants.showOneButtonAlertDialog("No notifications found", getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), NotificationActivity.this);
                                }
                                mAdapter = new NotificationAdapter(NotificationActivity.this, data, Language);
                                requestsList.setAdapter(mAdapter);
                                offernumber.setText(""+data.size());
                            } else {
                                //                          status false case
                                String failureResponse = verifyMobileResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), NotificationActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(NotificationActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(NotificationActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
                @Override
                public void onFailure(Call<NotificationResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(NotificationActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(NotificationActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            return null;
        }
        private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("UserId", userId);
                parentObj.put("Usertype","1");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }

    }

}
