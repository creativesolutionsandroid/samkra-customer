package com.cs.samkracustomer.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkracustomer.Adapters.InvoiceItemAdapter;
import com.cs.samkracustomer.Models.AccpetedOffersResponce;
import com.cs.samkracustomer.Models.InvoiceResponce;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InvoiceActivity extends AppCompatActivity {

    ImageView backBtn, share, print, download;
    TextView order_number, date, workshopadd, workshopadd1;
    TextView totalamt, vatamt, subamt, discountamt, netamt, finalamt;
    String TAG = "TAG";
    String Language = "En";
    ArrayList<AccpetedOffersResponce.Data> data = new ArrayList<>();
    String userId;

    int invoicePos;
    ScrollView mscrollview;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    LinearLayout itemsLayout;
    RelativeLayout rateLayout;
    TextView skipRating;
    RatingBar ratingBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.invoices);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        backBtn = findViewById(R.id.hy_back_btn);
        share = findViewById(R.id.share);
        download = findViewById(R.id.download);

        rateLayout = (RelativeLayout) findViewById(R.id.rate_layout);
        skipRating = (TextView) findViewById(R.id.skip_rating);
        ratingBar = (RatingBar) findViewById(R.id.ratingbar);

        totalamt = findViewById(R.id.totalamt);
        vatamt = findViewById(R.id.vatamt);
        subamt = findViewById(R.id.subamt);
        discountamt = findViewById(R.id.discountamt);
        netamt = findViewById(R.id.netamt);
        finalamt = findViewById(R.id.finalamt);
//        comment = findViewById(R.id.comment);
        order_number = findViewById(R.id.order_number);
        date = findViewById(R.id.date);
        workshopadd = findViewById(R.id.worker_shop_add);
        workshopadd1 = findViewById(R.id.worker_shop_add1);
        itemsLayout = (LinearLayout) findViewById(R.id.item_list);

        mscrollview = findViewById(R.id.scrollView);

        data = (ArrayList<AccpetedOffersResponce.Data>) getIntent().getSerializableExtra("data");
        invoicePos = getIntent().getIntExtra("pos", 0);

        String invoice = data.get(invoicePos).getInvoiceno();
        String[] invoice1 = invoice.split("-");

        order_number.setText("#" + invoice1[1]);

        String str = data.get(invoicePos).getCreatedon();
        String[] array = str.split(" ");

        date.setText(array[0]);
        workshopadd.setText("" + data.get(invoicePos).getWs().get(0).getWorkshopnameen());
        workshopadd1.setText(data.get(invoicePos).getWs().get(0).getAddress());
        totalamt.setText(Constants.priceFormat1.format(data.get(invoicePos).getTotalamount()));
        vatamt.setText(Constants.priceFormat1.format(data.get(invoicePos).getVatcharges()));
        subamt.setText(Constants.priceFormat1.format(data.get(invoicePos).getSubtotal()));
        discountamt.setText(Constants.priceFormat1.format(data.get(invoicePos).getDiscount()));
        netamt.setText(Constants.priceFormat1.format(data.get(invoicePos).getNetamount()));
        finalamt.setText(Constants.priceFormat1.format(data.get(invoicePos).getNetamount()));

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        itemsLayout.removeAllViews();
        for (int i = 0; i < data.get(invoicePos).getWs().get(0).getItems().size(); i++) {
            LayoutInflater inflater = (LayoutInflater) this.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
            View v = inflater.inflate(R.layout.invoice_list, null);
            TextView mitem = (TextView) v.findViewById(R.id.item);
            TextView amt = (TextView) v.findViewById(R.id.amt);

            mitem.setText(data.get(invoicePos).getWs().get(0).getItems().get(i).getItem());
            amt.setText(Constants.priceFormat.format(data.get(invoicePos).getWs().get(0).getItems().get(i).getItemtotal()));
            itemsLayout.addView(v);
        }

        skipRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rateLayout.setVisibility(View.GONE);
            }
        });

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                if(b){
                    Intent intent = new Intent(InvoiceActivity.this, ReviewActivity.class);
                    intent.putExtra("rId", data.get(invoicePos).getRequestid());
                    intent.putExtra("rCode", data.get(invoicePos).getRequestcode());
                    intent.putExtra("distance", data.get(invoicePos).getWs().get(0).getWorkshopdistance());
                    intent.putExtra("wId", data.get(invoicePos).getWorkshopid());
                    intent.putExtra("rating", v);
                    startActivityForResult(intent, 10);
                }
            }
        });

        if(!data.get(invoicePos).getIsrating()){
            rateLayout.setVisibility(View.VISIBLE);
        }
        else {
            rateLayout.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 10 && resultCode == RESULT_OK){
            rateLayout.setVisibility(View.GONE);
        }
        else {
            rateLayout.setVisibility(View.VISIBLE);
            ratingBar.setRating(0);
        }
    }
}
