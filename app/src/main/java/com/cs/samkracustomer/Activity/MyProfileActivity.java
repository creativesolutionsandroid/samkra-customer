package com.cs.samkracustomer.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkracustomer.Models.Signupresponse;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyProfileActivity extends Activity {

    TextView Signup;
    private static final int CAMERA_REQUEST = 1888;


    AlertDialog customDialog;
    ImageView edit_user,edit_email,edit_password;
    Button Upassword;
    Button Update;
    String strMobile, strPassword, strEmail, strUsername,strUserid;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    public static boolean isOTPVerified = false;
    String mLoginStatus;
    SharedPreferences languagePrefs;
    String language;
    EditText inputPassword, inputName, inputEmail, inputMobile;
    ImageView backBtn,profilepic,editprofile;


    private static final int PICK_IMAGE_FROM_GALLERY = 2;
    private static final int STORAGE_REQUEST = 5;
    private static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final String[] CAMERA_PERMS = {
            Manifest.permission.CAMERA
    };
    boolean isCamera = false;


    @Nullable
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.account_fragment);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.account_fragment);
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        backBtn = (ImageView) findViewById(R.id.back_btn);
        profilepic = (ImageView) findViewById(R.id.ac_profilrpic);
        editprofile = (ImageView) findViewById(R.id.ac_edit);
        inputName = (EditText) findViewById(R.id.username);
        inputEmail = (EditText) findViewById(R.id.email);
        inputMobile = (EditText) findViewById(R.id.phone);
        inputPassword = (EditText) findViewById(R.id.password);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        mLoginStatus = userPrefs.getString("login_status", "");
        strUserid = userPrefs.getString("userId", null);
        Update =(Button)findViewById(R.id.update);
        edit_user=(ImageView)findViewById(R.id.edit_user);
        edit_email=(ImageView)findViewById(R.id.edit_email);
        edit_password=(ImageView)findViewById(R.id.edit_password);

        inputName.setText( userPrefs.getString("name", null));
        inputMobile.setText("+" +  userPrefs.getString("mobile", null));
        inputEmail.setText( userPrefs.getString("email", null));

        edit_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputName.setEnabled(true);
            }
        });
        edit_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputPassword.setEnabled(true);
            }
        });
        edit_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputEmail.setEnabled(true);
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Update.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (validations()) {
                    new ForgotPasswordApi().execute();
                }
            }
        });

      edit_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyProfileActivity.this, ChangePassword.class);
                startActivity(intent);
            }
        });
        editprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               showtwoButtonsAlertDialog();
            }
        });


    }

    private boolean validations() {
        strUsername = inputName.getText().toString().trim();
        strEmail = inputEmail.getText().toString().trim();
        if (strUsername.length() == 0) {
            inputName.setError(getResources().getString(R.string.signup_msg_invalid_name));
            return false;
        } else if (strEmail.length() == 0) {
            inputEmail.setError(getResources().getString(R.string.signup_msg_enter_email));
            return false;
        } else if (!isValidEmail(strEmail)) {
            inputEmail.setError(getResources().getString(R.string.signup_msg_enter_email));
            return false;
        }
        return true;
    }

    public boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        inputName.setText( userPrefs.getString("name", null));
        inputMobile.setText("+" +  userPrefs.getString("mobile", null));
        inputEmail.setText( userPrefs.getString("email", null));
    }


    private class ForgotPasswordApi extends AsyncTask<String, Integer, String> {

        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareLoginJson();
            dialog = new ACProgressFlower.Builder(MyProfileActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(MyProfileActivity.this);
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<Signupresponse> call = apiService.updateProfile(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<Signupresponse>() {
                @Override
                public void onResponse(Call<Signupresponse> call, Response<Signupresponse> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        Signupresponse registrationResponse = response.body();
                        if (registrationResponse.getStatus()) {
//                          status true case
                            String userId = registrationResponse.getData().getUserid();
                            userPrefEditor.putString("userId", userId);
                            userPrefEditor.putString("name", registrationResponse.getData().getUsername());
                            userPrefEditor.putString("email", registrationResponse.getData().getEmail());
                            userPrefEditor.putString("mobile", registrationResponse.getData().getPhone());
                            userPrefEditor.commit();
                            inputName.setText( userPrefs.getString("name", null));
                            inputMobile.setText("+" +  userPrefs.getString("mobile", null));
                            inputEmail.setText( userPrefs.getString("email", null));
                            inputName.setEnabled(false);
                            inputEmail.setEnabled(false);
                        } else {
//                          status false case
                            String failureResponse = registrationResponse.getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), MyProfileActivity.this);
                        }
                    } else {
                        if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            Toast.makeText(MyProfileActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MyProfileActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<Signupresponse> call, Throwable t) {
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(MyProfileActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MyProfileActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (dialog != null) {
                dialog.dismiss();
            }
        }


        private String prepareLoginJson() {
            JSONObject parentObj = new JSONObject();

            try {
                parentObj.put("Phone", "966" + strMobile);
                parentObj.put("Password", strPassword);
                parentObj.put("Email", strEmail);
                parentObj.put("UserId", strUserid);
                parentObj.put("Username", strUsername);
                parentObj.put("DeviceToken", "-1");
                parentObj.put("Language", language);


            } catch (JSONException e) {
                e.printStackTrace();
            }

            return parentObj.toString();
        }
    }


        public void showtwoButtonsAlertDialog () {
            int currentapiVersion = android.os.Build.VERSION.SDK_INT;
            if (currentapiVersion >= Build.VERSION_CODES.M) {
                if (!canAccessCamera()) {
                    requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
                } else if (!canAccessStorage()) {
                    requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                }
            }
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MyProfileActivity.this);
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = getLayoutInflater();
            int layout = R.layout.alert_dialog;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);

            TextView title = (TextView) dialogView.findViewById(R.id.title);
            TextView desc = (TextView) dialogView.findViewById(R.id.desc);
            TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
            TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);

            title.setText("Samkra");
            no.setText("Gallery");
            yes.setText("Camera");
            desc.setText("Pick one from the options");

            yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   openCamera();
                    customDialog.dismiss();
                }
            });

            no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   openGallery();
                    customDialog.dismiss();
                }
            });

            customDialog = dialogBuilder.create();
            customDialog.show();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = customDialog.getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            lp.copyFrom(window.getAttributes());
            //This makes the dialog take up the full width
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int screenWidth = size.x;

            double d = screenWidth * 0.85;
            lp.width = (int) d;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
        }

        private boolean canAccessStorage() {
            return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
        }

        private boolean canAccessCamera() {
            return (hasPermission1(Manifest.permission.CAMERA));
        }

        private boolean hasPermission1(String perm) {
            return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(MyProfileActivity.this, perm));
        }

        public void openGallery() {
            int currentapiVersion = Build.VERSION.SDK_INT;
            if (currentapiVersion >= Build.VERSION_CODES.M) {

                if (!canAccessStorage()) {
                    requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                    isCamera=false;
                } else {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
                }
            } else {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
            }
        }

        @Override
        public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

            switch (requestCode) {
                case STORAGE_REQUEST:

                    if (canAccessStorage()) {
                        if(isCamera){
                            openCamera();
                        }
                        else {
                            openGallery();
                        }
                    }
                    else {
                        Toast.makeText(MyProfileActivity.this, "Storage permission denied, Unable to select pictures", Toast.LENGTH_LONG).show();
                    }
                    break;

                case CAMERA_REQUEST:
                    if (!canAccessStorage()) {
                        requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                    }
                    else {
                        openCamera();
                    }
                    break;
            }
        }

        public void openCamera() {
            int currentapiVersion = Build.VERSION.SDK_INT;
            if (currentapiVersion >= Build.VERSION_CODES.M) {

                if (!canAccessCamera()) {
                    isCamera=true;
                    requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
                } else {
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                }
            } else {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        }
}