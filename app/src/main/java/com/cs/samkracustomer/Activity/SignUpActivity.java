package com.cs.samkracustomer.Activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkracustomer.Dialogs.VerifyOtpDialog;
import com.cs.samkracustomer.Models.VerifyMobileResponse;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class SignUpActivity extends AppCompatActivity {

    EditText inputPassword,inputName,inputEmail,inputMobile,inputConfirmpassword;
    TextView textRegister;
    Button button;
    ImageView back_btn;
    String strName, strEmail, strMobile, strPassword ,strConfirmpassword;
    public static boolean isOTPVerified = false;
    AlertDialog customDialog;

    public static final String[] SMS_RECIEVER = {
            android.Manifest.permission.RECEIVE_SMS
    };
    public static final int SMS_REQUEST = 1;
    private static final String TAG = "TAG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        inputName = (EditText) findViewById(R.id.username);
        inputEmail = (EditText) findViewById(R.id.email);
        inputMobile = (EditText) findViewById(R.id.phone);
        inputPassword = (EditText) findViewById(R.id.password);
        inputConfirmpassword = (EditText) findViewById(R.id.confirm);
        Button inputButton = (Button) findViewById(R.id.bt1);
        back_btn =(ImageView)findViewById(R.id.back_btn);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt1:
                if (validations()) {
                    showtwoButtonsAlertDialog();
                }
                break;
        }
    }

    private void showtwoButtonsAlertDialog() {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SignUpActivity.this);
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = getLayoutInflater();
            int layout = R.layout.alert_dialog;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);

            TextView title = (TextView) dialogView.findViewById(R.id.title);
            TextView desc = (TextView) dialogView.findViewById(R.id.desc);
            TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
            TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);

            title.setText(R.string.opt_msg_verify);
            yes.setText(getResources().getString(R.string.ok));
            no.setText(getResources().getString(R.string.edit));
            desc.setText(getResources().getString(R.string.signup_alert_mobile_verify1)+"+966 "+strMobile+getResources().getString(R.string.signup_alert_mobile_verify2));

            yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(SignUpActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new verifyMobileApi().execute();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    customDialog.dismiss();
                }
            });

            no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Constants.requestEditTextFocus(inputMobile, SignUpActivity.this);
                    inputMobile.setSelection(inputMobile.length());
                    customDialog.dismiss();
                }
            });

            customDialog = dialogBuilder.create();
            customDialog.show();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = customDialog.getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            lp.copyFrom(window.getAttributes());
            //This makes the dialog take up the full width
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int screenWidth = size.x;

            double d = screenWidth*0.85;
            lp.width = (int) d;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
        }

    private boolean validations() {
        strName = inputName.getText().toString().trim();
        strMobile = inputMobile.getText().toString().trim();
        strPassword = inputPassword.getText().toString().trim();
        strEmail = inputEmail.getText().toString().trim();
        strConfirmpassword = inputConfirmpassword.getText().toString().trim();
        strMobile = strMobile.replace("+966 ","");
        if (strName.length() == 0) {
            inputName.setError(getResources().getString(R.string.signup_msg_invalid_name));
            return false;
        } else if (strMobile.length() == 0) {
            inputMobile.setError(getResources().getString(R.string.signup_msg_enter_mobile));
            return false;
        } else if (strMobile.length() != 9) {
            inputMobile.setError(getResources().getString(R.string.signup_msg_enter_mobile));
            return false;
        }
        else if (strEmail.length() == 0) {
            inputEmail.setError(getResources().getString(R.string.signup_msg_enter_email));
            return false;
        }
        else if (!isValidEmail(strEmail)) {
            inputEmail.setError(getResources().getString(R.string.signup_msg_enter_email));
            return false;
        }
        else if (strPassword.length() == 0) {
            inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            return false;
        }
        else if (strPassword.length() < 4 || strPassword.length() > 20) {
            inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            return false;
        }
        else if (strConfirmpassword.length() == 0) {
            inputConfirmpassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            return false;
        }
        else if(!strPassword.equals(strConfirmpassword) ){
            inputConfirmpassword.setError(getResources().getString(R.string.signup_msg_enter_match_password));
            return false;
        }
        return true;
    }

    public boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private class verifyMobileApi extends AsyncTask<String, String, String> {

        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            dialog = new ACProgressFlower.Builder(SignUpActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(SignUpActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<VerifyMobileResponse> call = apiService.verfiyMobileNumber(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<VerifyMobileResponse>() {
                @Override
                public void onResponse(Call<VerifyMobileResponse> call, Response<VerifyMobileResponse> response) {
                    if (response.isSuccessful()) {
                        VerifyMobileResponse verifyMobileResponse = response.body();
                        try {
                            if (verifyMobileResponse.getStatus()) {
                                Log.d(TAG, "onResponse: "+verifyMobileResponse.getData().getOtp());
                                showVerifyDialog();
                            } else {
                                //                          status false case
                                String failureResponse = verifyMobileResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), SignUpActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<VerifyMobileResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(SignUpActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            return null;
        }

        private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("Phone", "966" + strMobile);
                parentObj.put("Email", strEmail);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "prepareVerifyMobileJson: "+parentObj);
            return parentObj.toString();
        }

        private void showVerifyDialog() {
            Intent intent = new Intent(SignUpActivity.this, VerifyOtpDialog.class);
            intent.putExtra("name", strName);
            intent.putExtra("email", strEmail);
            intent.putExtra("mobile", strMobile);
            intent.putExtra("password", strPassword);
            intent.putExtra("screen", "register");
            startActivity(intent);
        }
    }
}
