package com.cs.samkracustomer.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkracustomer.Adapters.ImagesUploadedAdapter;
import com.cs.samkracustomer.Adapters.NewUserRequestsAdapter;
import com.cs.samkracustomer.Adapters.OffersAdapter;
import com.cs.samkracustomer.Models.GetUserNewRequestResponse;
import com.cs.samkracustomer.Models.OffersResponse;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OffersActivity extends Activity {

    ImageView backBtn;
    TextView count;
    ListView offersList;
    OffersAdapter mAdapter;
    ArrayList<OffersResponse.Data> data = new ArrayList<>();
    String Language = "En";
    String TAG = "TAG";
    String requestId = "";

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String userId;
    ArrayList<GetUserNewRequestResponse.Data> carData = new ArrayList<>();
    int carPos;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.receiving);

        requestId = getIntent().getStringExtra("id");
        carData = (ArrayList<GetUserNewRequestResponse.Data>) getIntent().getSerializableExtra("carData");
        carPos = getIntent().getIntExtra("carPos", 0);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        backBtn = (ImageView) findViewById(R.id.back_btn);
        count = (TextView) findViewById(R.id.bid_count);
        offersList = (ListView) findViewById(R.id.offers_list);

        offersList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(OffersActivity.this, OfferDetailsActivity.class);
                intent.putExtra("bidData", data);
                intent.putExtra("carData", carData);
                intent.putExtra("bidPos",i);
                intent.putExtra("carPos",carPos);
                startActivity(intent);
            }
        });

        String networkStatus = NetworkUtil.getConnectivityStatusString(OffersActivity.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new offersApi().execute();
        }
        else{
            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private class offersApi extends AsyncTask<String, String, String> {

        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            dialog = new ACProgressFlower.Builder(OffersActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(OffersActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<OffersResponse> call = apiService.OffersRequest(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<OffersResponse>() {
                @Override
                public void onResponse(Call<OffersResponse> call, Response<OffersResponse> response) {
                    if (response.isSuccessful()) {
                        OffersResponse verifyMobileResponse = response.body();
                        try {
                            if (verifyMobileResponse.getStatus()) {
                                data = verifyMobileResponse.getData();
                                if(data.size() == 0){
                                    Constants.showOneButtonAlertDialog("No bids found", getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), OffersActivity.this);
                                }
                                mAdapter = new OffersAdapter(OffersActivity.this, data, Language);
                                offersList.setAdapter(mAdapter);
                                count.setText(""+data.size());
                            } else {
                                //                          status false case
                                String failureResponse = verifyMobileResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), OffersActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(OffersActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(OffersActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<OffersResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(OffersActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(OffersActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            return null;
        }

        private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("RequestId", requestId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }
    }
}
