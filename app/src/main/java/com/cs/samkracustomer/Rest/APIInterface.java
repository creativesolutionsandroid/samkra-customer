package com.cs.samkracustomer.Rest;

import com.cs.samkracustomer.Models.AccpetedOffersResponce;
import com.cs.samkracustomer.Models.Carmakersmodelresponse;
import com.cs.samkracustomer.Models.GetUserNewRequestResponse;
import com.cs.samkracustomer.Models.InvoiceResponce;
import com.cs.samkracustomer.Models.NotificationResponce;
import com.cs.samkracustomer.Models.OffersResponse;
import com.cs.samkracustomer.Models.PlacebidResponce;
import com.cs.samkracustomer.Models.ReviewResponse;
import com.cs.samkracustomer.Models.SaveUserRequestResponse;
import com.cs.samkracustomer.Models.Signupresponse;
import com.cs.samkracustomer.Models.VerifyMobileResponse;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface APIInterface {

    @POST("UsersAPI/VerfiyMobileEmail")
    Call<VerifyMobileResponse> verfiyMobileNumber(@Body RequestBody body);

    @POST("UsersAPI/SignupUser")
    Call<Signupresponse> userRegistration(@Body RequestBody body);

    @POST("UsersAPI/SignInUser")
    Call<Signupresponse> userLogin(@Body RequestBody body);

    @POST("UsersAPI/ForgotPassword")
    Call<VerifyMobileResponse> forgotPassword(@Body RequestBody body);

    @POST("UsersAPI/SetNewPassword")
    Call<Signupresponse> resetPassword(@Body RequestBody body);

    @POST("UsersAPI/UpdateProfile")
    Call<Signupresponse> updateProfile(@Body RequestBody body);

    @POST("UsersAPI/ResetPassword")
    Call<Signupresponse> changepassword(@Body RequestBody body);

    @GET("UsersAPI/GetCarMakersModels")
    Call<Carmakersmodelresponse> Carmakers();

    @POST("UsersAPI/SaveUserRequest")
    Call<SaveUserRequestResponse> saveUserRequest(@Body RequestBody body);

    @POST("UsersAPI/GetUserNewRequest")
    Call<GetUserNewRequestResponse> GetUserNewRequest(@Body RequestBody body);

    @POST("UsersAPI/GetUserRequestOfferList")
    Call<OffersResponse> OffersRequest(@Body RequestBody body);

    @POST("UsersAPI/AcceptUserBid")
    Call<PlacebidResponce> userAccpet(@Body RequestBody body);

    @POST("UsersAPI/GetListInvoices")
    Call<AccpetedOffersResponce> historylist(@Body RequestBody body);

    @POST("UsersAPI/AcceptUserFinalBidPrice")
    Call<PlacebidResponce> AcceptUserFinalBidPrice(@Body RequestBody body);

    @POST("WorkshopAppAPI/GetListNotifications")
    Call<NotificationResponce> Notification(@Body RequestBody body);

    @POST("UsersAPI/GetListInvoices")
    Call<InvoiceResponce> InvoiceRequest(@Body RequestBody body);

    @POST("UsersAPI/SaveRatings")
    Call<ReviewResponse> RatingRequst(@Body RequestBody body);

}
